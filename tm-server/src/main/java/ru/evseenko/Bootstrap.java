package ru.evseenko;

import ru.evseenko.api.service.ProjectService;
import ru.evseenko.api.service.TaskService;
import ru.evseenko.api.service.UserService;
import ru.evseenko.endpoint.Constant;
import ru.evseenko.endpoint.ProjectEndpoint;
import ru.evseenko.endpoint.TaskEndpoint;
import ru.evseenko.endpoint.UserEndpoint;
import ru.evseenko.service.ProjectServiceImp;
import ru.evseenko.service.SessionService;
import ru.evseenko.service.TaskServiceImp;
import ru.evseenko.service.UserServiceImp;

import javax.xml.ws.Endpoint;


public class Bootstrap {

    private final SessionService sessionService = new SessionService();
    private final ProjectService projectService = new ProjectServiceImp();
    private final TaskService taskService = new TaskServiceImp();
    private final UserService userService = new UserServiceImp();

    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService);
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService, sessionService);
    private final UserEndpoint userEndpoint = new UserEndpoint(userService, sessionService);

    public void init() {
        projectService.setTaskService(taskService);

        Endpoint.publish(Constant.PROJECT_ENDPOINT_URL, projectEndpoint);
        Endpoint.publish(Constant.TASK_ENDPOINT_URL, taskEndpoint);
        Endpoint.publish(Constant.USER_ENDPOINT_URL, userEndpoint);
    }
}
