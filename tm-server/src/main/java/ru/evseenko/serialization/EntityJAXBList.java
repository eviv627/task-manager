package ru.evseenko.serialization;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Task;
import ru.evseenko.entity.User;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="entities")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlSeeAlso({Project.class, Task.class, User.class})
public class EntityJAXBList<T> {
    private List<T> entities = new ArrayList<>();

    @NotNull
    @XmlElement(required = true, name="entity")
    public List<T> getEntities() {
        return entities;
    }

    public void setEntities(@NotNull List<T> entities) {
        this.entities = entities;
    }
}
