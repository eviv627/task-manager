package ru.evseenko.serialization;

public class SerializationUtil<T> {

//    public void serializationSave(@NotNull final Session session) {
//        sessionService.checkSession(session);
//
//        final List<T> entities = getRepository().findAll(session.getUserUUID());
//        if (entities.size() == 0) {
//            return;
//        }
//
//        FileOutputStream fos = null;
//        ObjectOutputStream oos = null;
//
//        try {
//            fos = new FileOutputStream(session.getUserUUID() + this.getClass().getName() + ".out");
//            oos = new ObjectOutputStream(fos);
//
//            for (T entity : entities) {
//                oos.writeObject(entity);
//                oos.flush();
//            }
//
//        } catch (IOException e) {
//            throw new ServiceException(e);
//        }
//        finally {
//            if (oos != null) {
//                try {
//                    oos.close();
//                } catch (IOException e) {
//                    throw new ServiceException(e);
//                }
//            }
//            if (fos != null) {
//                try {
//                    fos.close();
//                } catch (IOException e) {
//                    throw new ServiceException(e);
//                }
//            }
//        }
//    }
//
//    public void serializationLoad(@NotNull final Session session) {
//        sessionService.checkSession(session);
//        final List<T> entities = new ArrayList<>();
//
//        FileInputStream fis = null;
//        ObjectInputStream ois = null;
//
//        try {
//            fis = new FileInputStream(session.getUserUUID() + this.getClass().getName() + ".out");
//            ois = new ObjectInputStream(fis);
//            while (true) {
//                entities.add((T) ois.readObject());
//            }
//        } catch (EOFException e) {
//            // ignore
//        } catch (ClassNotFoundException | IOException e) {
//            throw new ServiceException(e);
//        } finally {
//            if (ois != null) {
//                try {
//                    ois.close();
//                } catch (IOException e) {
//                    throw new ServiceException(e);
//                }
//            }
//            if (fis != null) {
//                try {
//                    fis.close();
//                } catch (IOException e) {
//                    throw new ServiceException(e);
//                }
//            }
//        }
//
//        for (T entity : entities) {
//            getRepository().persist(session.getUserUUID(), entity);
//        }
//    }
//
//    public void xmlJaxbSave(@NotNull final Session session) {
//        sessionService.checkSession(session);
//
//        final File file = new File(session.getUserUUID() + this.getClass().getName() + "_jaxb.xml");
//        final EntityJAXBList<T> list = new EntityJAXBList<>();
//        list.setEntities(getRepository().findAll(session.getUserUUID()));
//
//        try {
//            final JAXBContext context = JAXBContext.newInstance(list.getClass());
//            final Marshaller marshaller = context.createMarshaller();
//            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//
//            marshaller.marshal(list, file);
//        } catch (JAXBException e) {
//            throw new ServiceException(e);
//        }
//    }
//
//    public void xmlJaxbLoad(@NotNull final Session session) {
//        sessionService.checkSession(session);
//
//        final File file = new File(session.getUserUUID() + this.getClass().getName() + "_jaxb.xml");
//        EntityJAXBList<T> list = new EntityJAXBList<>();
//        List<T> entities;
//
//        try {
//            final JAXBContext context = JAXBContext.newInstance(list.getClass());
//            final Unmarshaller unmarshaller = context.createUnmarshaller();
//
//            list = (EntityJAXBList<T>) unmarshaller.unmarshal(file);
//            entities = list.getEntities();
//        } catch (JAXBException e) {
//            throw new ServiceException(e);
//        }
//
//        for (T entity : entities) {
//            getRepository().persist(session.getUserUUID(), entity);
//        }
//    }
//
//    public void jsonSave(@NotNull final Session session) {
//        sessionService.checkSession(session);
//
//        final ObjectMapper mapper = new ObjectMapper();
//        mapper.enable(SerializationFeature.INDENT_OUTPUT);
//        final File file = new File(session.getUserUUID() + this.getClass().getName() + ".json");
//
//        try {
//            mapper.writeValue(file, getRepository().findAll(session.getUserUUID()));
//        } catch (IOException e) {
//            throw new ServiceException(e);
//        }
//    }
//
//    public void jsonLoad(@NotNull final Session session) {
//        sessionService.checkSession(session);
//
//        final ObjectMapper mapper = new ObjectMapper();
//        mapper.enable(SerializationFeature.INDENT_OUTPUT);
//        final File file = new File(session.getUserUUID() + this.getClass().getName() + ".json");
//
//        final List<T> entities;
//        try {
//            entities = mapper.readValue(file, getTypeReference());
//        } catch (IOException e) {
//            throw new ServiceException(e);
//        }
//
//        for (T entity : entities) {
//            getRepository().persist(session.getUserUUID(), entity);
//        }
//    }
//
//    public void jsonMoxySave(@NotNull final Session session) {
//        sessionService.checkSession(session);
//
//        final File file = new File(session.getUserUUID() + this.getClass().getName() + "_moxy.json");
//        final EntityJAXBList<T> list = new EntityJAXBList<>();
//        list.setEntities(getRepository().findAll(session.getUserUUID()));
//
//        try {
//            final JAXBContext context = JAXBContext.newInstance(EntityJAXBList.class);
//
//            final Marshaller marshaller = context.createMarshaller();
//
//            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//            marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
//            marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
//
//            marshaller.marshal(list, file);
//        } catch (JAXBException e) {
//            throw new ServiceException(e);
//        }
//    }
//
//    public void jsonMoxyLoad(@NotNull final Session session) {
//        sessionService.checkSession(session);
//
//        final File file = new File(session.getUserUUID() + this.getClass().getName() + "_moxy.json");
//        EntityJAXBList<T> list = new EntityJAXBList<>();
//        List<T> entities;
//
//        try {
//
//            final JAXBContext context = JAXBContext.newInstance(list.getClass());
//            final Unmarshaller unmarshaller = context.createUnmarshaller();
//
//            unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
//            unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
//
//            list = (EntityJAXBList<T>) unmarshaller.unmarshal(file);
//            entities = list.getEntities();
//
//        } catch (JAXBException e) {
//            throw new ServiceException(e);
//        }
//
//        for (T entity : entities) {
//            getRepository().persist(session.getUserUUID(), entity);
//        }
//    }
//
//    public void xmlJacksonSave(@NotNull final Session session) {
//        sessionService.checkSession(session);
//
//        final XmlMapper mapper = new XmlMapper();
//        mapper.enable(SerializationFeature.INDENT_OUTPUT);
//        final File file = new File(session.getUserUUID() + this.getClass().getName() + "_jackson.xml");
//        try {
//            mapper.writeValue(file, getRepository().findAll(session.getUserUUID()));
//        } catch (IOException e) {
//            throw new ServiceException(e);
//        }
//    }
//
//    public void xmlJacksonLoad(@NotNull final Session session) {
//        sessionService.checkSession(session);
//
//        final XmlMapper mapper = new XmlMapper();
//        mapper.enable(SerializationFeature.INDENT_OUTPUT);
//        final File file = new File(session.getUserUUID() + this.getClass().getName() + "_jackson.xml");
//
//        final List<T> entities;
//        try {
//            entities = mapper.readValue(file, getTypeReference());
//        } catch (IOException e) {
//            throw new ServiceException(e);
//        }
//
//        for (T entity : entities) {
//            getRepository().persist(session.getUserUUID(), entity);
//        }
//    }
}
