package ru.evseenko.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.repository.Repository;
import ru.evseenko.api.repository.UserRepository;
import ru.evseenko.api.repository.mybatis.UserRepositoryMyBatis;
import ru.evseenko.api.service.UserService;
import ru.evseenko.entity.User;
import ru.evseenko.exception.ServiceException;
import ru.evseenko.util.MyBatisUtil;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
public class UserServiceImp implements UserService {

    @NotNull
    private UserRepository getRepository(SqlSession session){
        return session.getMapper(UserRepositoryMyBatis.class);
    }

    @NotNull
    private SqlSession getSession() {
        return MyBatisUtil.getSqlSessionFactory().openSession();
    }


    @Override
    public void persist(@NotNull final User entity) {
        @NotNull final SqlSession session = getSession();

        final List<User> users = getRepository(session).findAll();

        for (User user : users) {
            if(user.getLogin() != null && user.getLogin().equals(entity.getLogin())) {
                throw new ServiceException("Login is busy");
            }
        }

        try {
            @NotNull final UserRepository repository = getRepository(session);

            entity.setId(UUID.randomUUID().toString());
            repository.persist(entity);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw new ServiceException(e);
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public User getUserByLogin(@NotNull final String login) {
        @NotNull final SqlSession session = getSession();

        final List<User> users = getRepository(session).findAll();

        for (User user : users) {
            if(login.equals(user.getLogin())) {
                return user;
            }
        }
        throw new ServiceException("No such user");
    }

    @NotNull
    public User get(@NotNull final String id) {
        try(final SqlSession session = getSession()) {
            @NotNull final UserRepository repository = getRepository(session);
            return repository.findOne(id);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(@NotNull User entity) {
        @NotNull final SqlSession session = getSession();
        try {
            @NotNull final UserRepository repository = getRepository(session);
            repository.merge(entity);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw new ServiceException(e);
        } finally {
            session.close();
        }
    }
}
