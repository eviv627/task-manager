package ru.evseenko.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.repository.ProjectRepository;
import ru.evseenko.api.repository.mybatis.ProjectRepositoryMyBatis;
import ru.evseenko.api.service.ProjectService;
import ru.evseenko.api.service.TaskService;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Task;

import java.util.List;

@NoArgsConstructor
public class ProjectServiceImp extends AbstractEntityService<Project> implements ProjectService {

    private TaskService taskService;

    public void setTaskService(@NotNull final TaskService taskService) {
        this.taskService = taskService;
    }

    @NotNull
    @Override
    protected ProjectRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(ProjectRepositoryMyBatis.class);
    }

    @Override
    public void delete(@NotNull final String userId, @NotNull final Project entity) {
        final List<Task> tasks = taskService.getTaskForProject(userId, entity);
        for (Task task : tasks) {
            taskService.delete(userId, task);
        }
        super.delete(userId, entity);
    }

    @Override
    public void deleteAll(@NotNull final String userId) {
        taskService.deleteAllRelated(userId);
        super.deleteAll(userId);
    }
}
