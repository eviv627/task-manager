package ru.evseenko.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.repository.SessionRepository;
import ru.evseenko.api.repository.mybatis.SessionRepositoryMyBatis;
import ru.evseenko.entity.Session;
import ru.evseenko.exception.ServiceException;
import ru.evseenko.util.MyBatisUtil;

@NoArgsConstructor
public class SessionService {

    public void checkSession(@NotNull final Session session) {
        final String signature;
        @NotNull final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            SessionRepository repository = sqlSession.getMapper(SessionRepositoryMyBatis.class);
            signature = repository.findOne(session.getUserUUID(), session.getId()).getSignature();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new ServiceException(e);
        } finally {
            sqlSession.close();
        }

        if (!session.getSignature().equals(signature)) {
            throw new ServiceException("authentication fail");
        }
    }

    public void setUpSession(@NotNull final Session session) {
        @NotNull final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            SessionRepository repository = sqlSession.getMapper(SessionRepositoryMyBatis.class);
            repository.persist(session.getUserUUID(), session);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new ServiceException(e);
        } finally {
            sqlSession.close();
        }
    }

    public void tearDownSession(@NotNull final Session session) {
        @NotNull final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            SessionRepository repository = sqlSession.getMapper(SessionRepositoryMyBatis.class);
            repository.remove(session.getUserUUID(), session);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new ServiceException(e);
        } finally {
            sqlSession.close();
        }
    }
}
