package ru.evseenko.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.entity.Entity;
import ru.evseenko.api.repository.Repository;
import ru.evseenko.api.service.EntityService;
import ru.evseenko.exception.ServiceException;
import ru.evseenko.util.MyBatisUtil;

import java.util.List;
import java.util.UUID;

public abstract class AbstractEntityService<T extends Entity> implements EntityService<T> {

    @NotNull protected abstract Repository<T> getRepository(SqlSession session);

    @NotNull protected SqlSession getSession() {
        return MyBatisUtil.getSqlSessionFactory().openSession();
    }

    @NotNull
    @Override
    public T get(@NotNull final String userId, @NotNull final String id) {
        try(final SqlSession session = getSession()) {
            final Repository<T> repository = getRepository(session);
            return repository.findOne(userId, id);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @NotNull
    @Override
    public List<T> getAll(@NotNull final String userId) {
        try(final SqlSession session = getSession()) {
            final Repository<T> repository = getRepository(session);
            return repository.findAll(userId);
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final T entity) {
        @NotNull final SqlSession session = getSession();
        try {
            final Repository<T> repository = getRepository(session);
            repository.merge(userId, entity);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw new ServiceException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void persist(@NotNull final String userId, @NotNull final T entity) {
        @NotNull final SqlSession session = getSession();
        try {
            final Repository<T> repository = getRepository(session);

            entity.setId(UUID.randomUUID().toString());
            repository.persist(userId, entity);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw new ServiceException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(@NotNull final String userId, @NotNull final T entity) {
        @NotNull final SqlSession session = getSession();
        try {
            final Repository<T> repository = getRepository(session);
            repository.remove(userId, entity);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw new ServiceException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteAll(@NotNull final String userId) {
        @NotNull final SqlSession session = getSession();
        try {
            final Repository<T> repository = getRepository(session);
            repository.removeAll(userId);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw new ServiceException(e);
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public List<T> findByName(@NotNull final String userId, @NotNull final String name) {
        final List<T> entities = getAll(userId);
        entities.removeIf(t -> !t.getName().contains(name));
        return entities;
    }

    @NotNull
    @Override
    public List<T> findByDescription(@NotNull final String userId, @NotNull final String description) {
        final List<T> entities = getAll(userId);

        entities.removeIf(t -> !t.getDescription().contains(description));
        return entities;
    }
}
