package ru.evseenko.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.repository.TaskRepository;
import ru.evseenko.api.repository.mybatis.TaskRepositoryMyBatis;
import ru.evseenko.api.service.TaskService;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Task;

import java.util.List;

@NoArgsConstructor
public class TaskServiceImp extends AbstractEntityService<Task> implements TaskService {

    @NotNull
    @Override
    protected TaskRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(TaskRepositoryMyBatis.class);
    }

    @NotNull
    @Override
    public List<Task> getTaskForProject(@NotNull final String userId, @NotNull final Project project) {
        final List<Task> tasks = getAll(userId);
        tasks.removeIf(task -> {
            assert project.getId() != null;
            return !project.getId().equals(task.getProjectId());
        });
        return tasks;
    }

    @Override
    public void deleteAllRelated(@NotNull final String userId) {
        final List<Task> tasks = getAll(userId);
        tasks.removeIf(task -> task.getProjectId() == null);
        for (Task task : tasks) {
            super.delete(userId, task);
        }
    }
}
