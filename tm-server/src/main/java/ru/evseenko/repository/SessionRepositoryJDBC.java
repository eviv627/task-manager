package ru.evseenko.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.api.repository.SessionRepository;
import ru.evseenko.entity.Session;
import ru.evseenko.exception.RepositoryException;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SessionRepositoryJDBC extends AbstractRepositoryJDBC<Session> implements SessionRepository {

    public SessionRepositoryJDBC() {
        super();
    }

    @Override
    String getInsertQuery() {
        return "INSERT INTO app_session (id, user_id, signature, create_date) " +
                "VALUES (?, ?, ?, ?);";
    }

    @Override
    String getDeleteQuery() {
        return "DELETE FROM app_session WHERE id = ? AND user_id = ?;";
    }

    @Override
    String getDeleteAllQuery() {
        throw new UnsupportedOperationException("Can not delete all session");
    }

    @Override
    String getSelectQuery() {
        return "SELECT * FROM app_session " +
                "WHERE id = ? " +
                "AND user_id = ?;";
    }

    @Override
    String getSelectAllQuery() {
        throw new UnsupportedOperationException("Can not select all session");
    }

    @Override
    String getUpdateQuery() {
        throw new UnsupportedOperationException("Can not update session");
    }

    @Override
    void makeInsertStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final Session entity,
            @Nullable final String userId
    ) {
        assert userId != null;
        try {
            statement.setString(1, entity.getId());
            statement.setString(2, entity.getUserUUID());
            statement.setString(3, entity.getSignature());
            statement.setDate(4, new Date(entity.getCreateDate().getTime()));
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    void makeUpdateStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final Session entity,
            @NotNull final String userId
    ) {
        throw new UnsupportedOperationException("Can not update session");
    }

    @Override
    void makeSelectAllStatement(
            @NotNull final PreparedStatement statement,
            @Nullable final String userId
    ) {
        throw new UnsupportedOperationException("Can not select all session");
    }

    @Override
    Session parseResultSet(@NotNull final ResultSet resultSet) {
        try {
            @NotNull final Session session = new Session();

            session.setId(resultSet.getString("id"));
            session.setUserUUID(resultSet.getString("user_id"));
            session.setSignature(resultSet.getString("signature"));
            session.setCreateDate(resultSet.getDate("create_date"));

            return session;
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }
}
