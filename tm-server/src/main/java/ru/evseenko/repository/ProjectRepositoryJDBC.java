package ru.evseenko.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.api.repository.ProjectRepository;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Status;
import ru.evseenko.exception.RepositoryException;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProjectRepositoryJDBC extends AbstractRepositoryJDBC<Project> implements ProjectRepository {

    public ProjectRepositoryJDBC() {
        super();
    }

    @Override
    @NotNull
    String getInsertQuery() {
        return "INSERT INTO app_project (id, name, description, start_date, end_date, " +
                "create_date, status, user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
    }

    @Override
    @NotNull
    String getDeleteQuery() {
        return "DELETE FROM app_project WHERE id = ? AND user_id = ?;";
    }

    @Override
    @NotNull
    String getDeleteAllQuery() {
        return "DELETE FROM app_project WHERE user_id = ?;";
    }

    @Override
    @NotNull
    String getSelectQuery() {
        return "SELECT * FROM app_project " +
                "WHERE id = ? " +
                "AND user_id = ?;";
    }

    @Override
    @NotNull
    String getSelectAllQuery() {
        return "SELECT * FROM app_project " +
                "WHERE user_id = ?;";
    }

    @Override
    @NotNull
    String getUpdateQuery() {
        return "UPDATE app_project " +
                "SET name = ?, description = ?, start_date = ?, end_date = ?, " +
                "create_date = ?, status = ? " +
                "WHERE id = ? AND user_id = ?;";
    }

    @Override
    void makeInsertStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final Project entity,
            @Nullable final String userId
    ) {
        assert userId != null;
        try {
            assert entity.getStatus() != null;
            statement.setString(1, entity.getId());
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getDescription());

            final java.util.Date startDate = entity.getStartDate();
            statement.setDate(4, (startDate != null) ? new Date(startDate.getTime()) : null);

            final java.util.Date endDate = entity.getEndDate();
            statement.setDate(5, (endDate != null) ? new Date(endDate.getTime()) : null);

            final java.util.Date createDate = entity.getCreateDate();
            statement.setDate(6, (createDate != null) ? new Date(createDate.getTime()) : null);

            statement.setString(7, entity.getStatus().getName());
            statement.setString(8, entity.getUserId());
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    void makeUpdateStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final Project entity,
            @NotNull final String userId
    ) {
        try {
            assert entity.getStatus() != null;
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getDescription());

            final java.util.Date startDate = entity.getStartDate();
            statement.setDate(3, (startDate != null) ? new Date(startDate.getTime()) : null);

            final java.util.Date endDate = entity.getEndDate();
            statement.setDate(4, (endDate != null) ? new Date(endDate.getTime()) : null);

            final java.util.Date createDate = entity.getCreateDate();
            statement.setDate(5, (createDate != null) ? new Date(createDate.getTime()) : null);

            statement.setString(6, entity.getStatus().getName());
            statement.setString(7, entity.getId());
            statement.setString(8, entity.getUserId());
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    void makeSelectAllStatement(
            @NotNull final PreparedStatement statement,
            @Nullable final String userId
    ) {
        assert userId != null;
        try {
            statement.setString(1, userId);
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    @NotNull
    Project parseResultSet(@NotNull final ResultSet resultSet) {
        try {
            @NotNull final Project project = new Project();

            project.setId(resultSet.getString("id"));
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setStartDate(resultSet.getDate("start_date"));
            project.setEndDate(resultSet.getDate("end_date"));
            project.setCreateDate(resultSet.getDate("create_date"));
            project.setStatus(Status.valueOf(resultSet.getString("status")));
            project.setUserId(resultSet.getString("user_id"));
            return project;

        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }
}
