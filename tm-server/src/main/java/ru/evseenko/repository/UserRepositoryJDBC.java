package ru.evseenko.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.api.repository.UserRepository;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.User;
import ru.evseenko.exception.RepositoryException;
import ru.evseenko.util.ConnectionUtilJDBC;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public class UserRepositoryJDBC implements UserRepository {

    private String getInsertQuery() {
        return "INSERT INTO app_user (id, login, password_hash, role) " +
                "VALUES (?, ?, ?, ?);";
    }

    private String getDeleteQuery() {
        return "DELETE FROM app_user WHERE id = ?;";
    }

    private String getSelectQuery() {
        return "SELECT * FROM app_user " +
                "WHERE id = ?;";
    }

    private String getSelectAllQuery() {
        return "SELECT * FROM app_user;";
    }

    private String getUpdateQuery() {
        return "UPDATE app_user " +
                "SET login = ?, password_hash = ?, role = ? " +
                "WHERE id = ?;";
    }

    private void makeInsertStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final User entity
    ) {
        assert entity.getRole() != null;
        try {
            statement.setString(1, entity.getId());
            statement.setString(2, entity.getLogin());
            statement.setString(3, entity.getPasswordHash());
            statement.setString(4, entity.getRole().getName());
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    private void makeUpdateStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final User entity
    ) {
        assert entity.getRole() != null;
        try {
            statement.setString(1, entity.getLogin());
            statement.setString(2, entity.getPasswordHash());
            statement.setString(3, entity.getRole().getName());

            statement.setString(4, entity.getId());
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    private User parseResultSet(@NotNull final ResultSet resultSet) {
        try {
            @NotNull final User user = new User();

            user.setId(resultSet.getString("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPasswordHash(resultSet.getString("password_hash"));
            user.setRole(Role.valueOf(resultSet.getString("role")));

            return user;
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    private void makeStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final String entityId
    ) throws SQLException {
        statement.setString(1, entityId);
    }

    @NotNull
    private Connection getConnection() {
        return ConnectionUtilJDBC.getConnection();
    }

    private void closeConnection(@NotNull final Connection connection) {
        ConnectionUtilJDBC.closeConnection(connection);
    }

    @NotNull
    @Override
    public User findOne(
            @NotNull final String id
    ) {
        @NotNull final Connection connection = getConnection();
        @NotNull final User entity;

        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getSelectQuery())) {
            makeStatement(statement, id);
            try(@NotNull final ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    entity = parseResultSet(resultSet);
                } else {
                    throw new RepositoryException("No such key in database");
                }
            }
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }

        closeConnection(connection);
        return entity;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final Connection connection = getConnection();
        @Nullable ResultSet resultSet = null;

        @NotNull final List<User> listObj = new LinkedList<>();
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getSelectAllQuery())) {
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                @NotNull final User entity = parseResultSet(resultSet);
                listObj.add(entity);
            }
        } catch (SQLException e) {
            throw new RepositoryException(e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException e) {
                throw new RepositoryException(e);
            }
        }

        closeConnection(connection);
        return listObj;
    }

    @Override
    public void persist(
            @NotNull final User entity
    ) {
        @NotNull final Connection connection = getConnection();

        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getInsertQuery(), Statement.RETURN_GENERATED_KEYS)) {
            makeInsertStatement(statement, entity);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }

        closeConnection(connection);
    }

    @Override
    public void merge(
            @NotNull final User entity
    ) {
        @NotNull final Connection connection = getConnection();

        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getUpdateQuery())) {
            makeUpdateStatement(statement, entity);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }

        closeConnection(connection);
    }

    @Override
    public void remove(
            @NotNull final User entity
    ) {
        assert entity.getId() != null;
        @NotNull final Connection connection = getConnection();

        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getDeleteQuery())) {
            makeStatement(statement, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }

        closeConnection(connection);
    }
}
