package ru.evseenko.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.api.entity.Identifiable;
import ru.evseenko.api.repository.Repository;
import ru.evseenko.exception.RepositoryException;
import ru.evseenko.util.ConnectionUtilJDBC;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractRepositoryJDBC<T extends Identifiable> implements Repository<T> {

    abstract String getInsertQuery();

    abstract String getDeleteQuery();

    abstract String getDeleteAllQuery();

    abstract String getSelectQuery();

    abstract String getSelectAllQuery();

    abstract String getUpdateQuery();

    abstract void makeInsertStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final T entity,
            @Nullable final String userId
    );

    abstract void makeUpdateStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final T entity,
            @NotNull final String userId
    );

    abstract void makeSelectAllStatement(
            @NotNull final PreparedStatement statement,
            @Nullable final String userId
    );

    abstract T parseResultSet(
            @NotNull final ResultSet resultSet
    );

    private Connection getConnection() {
        return ConnectionUtilJDBC.getConnection();
    }

    private void closeConnection(Connection connection) {
        ConnectionUtilJDBC.closeConnection(connection);
    }

    private void makeStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final String entityId,
            @NotNull final String userId
    ) throws SQLException {
        statement.setString(1, entityId);
        statement.setString(2, userId);
    }

    @NotNull
    @Override
    public T findOne(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final Connection connection = getConnection();
        @NotNull final T entity;

        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getSelectQuery())) {
            makeStatement(statement, id, userId);
            try(@NotNull final ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    entity = parseResultSet(resultSet);
                } else {
                    throw new RepositoryException("No such key in database");
                }
            }
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }

        closeConnection(connection);
        return entity;
    }

    @NotNull
    @Override
    public List<T> findAll(@NotNull final String userId) {
        @NotNull final Connection connection = getConnection();
        @Nullable ResultSet resultSet = null;

        @NotNull final List<T> listObj = new LinkedList<>();
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getSelectAllQuery())) {
            makeSelectAllStatement(statement, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                @NotNull final T entity = parseResultSet(resultSet);
                listObj.add(entity);
            }
        } catch (SQLException e) {
            throw new RepositoryException(e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException e) {
                throw new RepositoryException(e);
            }
        }

        closeConnection(connection);
        return listObj;
    }

    @Override
    public void persist(
            @NotNull final String userId,
            @NotNull final T entity
    ) {
        @NotNull final Connection connection = getConnection();

        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getInsertQuery(), Statement.RETURN_GENERATED_KEYS)) {
            makeInsertStatement(statement, entity, userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }

        closeConnection(connection);
    }

    @Override
    public void merge(
            @NotNull final String userId,
            @NotNull final  T entity
    ) {
        @NotNull final Connection connection = getConnection();

        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getUpdateQuery())) {
            makeUpdateStatement(statement, entity, userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }

        closeConnection(connection);
    }

    @Override
    public void remove(
            @NotNull final String userId,
            @NotNull final T entity
    ) {
        @NotNull final Connection connection = getConnection();

        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getDeleteQuery())) {
            makeStatement(statement, entity.getId(), userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }

        closeConnection(connection);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final Connection connection = getConnection();

        try (@NotNull final PreparedStatement statement = connection.prepareStatement(getDeleteAllQuery())) {
            statement.setString(1, userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }

        closeConnection(connection);
    }
}
