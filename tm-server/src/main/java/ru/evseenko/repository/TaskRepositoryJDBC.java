package ru.evseenko.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.api.repository.TaskRepository;
import ru.evseenko.entity.Status;
import ru.evseenko.entity.Task;
import ru.evseenko.exception.RepositoryException;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TaskRepositoryJDBC extends AbstractRepositoryJDBC<Task> implements TaskRepository {

    public TaskRepositoryJDBC() {
        super();
    }

    @Override
    @NotNull
    String getInsertQuery() {
        return "INSERT INTO app_task (id, name, description, start_date, end_date, " +
                "create_date, status, project_id, user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
    }

    @Override
    @NotNull
    String getDeleteQuery() {
        return "DELETE FROM app_task WHERE id = ? AND user_id = ?;";
    }

    @Override
    @NotNull
    String getDeleteAllQuery() {
        return "DELETE FROM app_task WHERE user_id = ?;";
    }

    @Override
    @NotNull
    String getSelectQuery() {
        return "SELECT * FROM app_task " +
                "WHERE id = ?" +
                "AND user_id = ?;";
    }

    @Override
    @NotNull
    String getSelectAllQuery() {
        return "SELECT * FROM app_task " +
                "WHERE user_id = ?;";
    }

    @Override
    @NotNull
    String getUpdateQuery() {
        return "UPDATE app_task " +
                "SET name = ?, description = ?, start_date = ?, end_date = ?, " +
                "create_date = ?, status = ?, project_id = ?" +
                "WHERE id = ? AND user_id = ?;";
    }

    @Override
    void makeInsertStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final Task entity,
            @Nullable final String userId
    ) {
        assert userId != null;
        try {
            assert entity.getStatus() != null;
            statement.setString(1, entity.getId());
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getDescription());

            final java.util.Date startDate = entity.getStartDate();
            statement.setDate(4, (startDate != null) ? new Date(startDate.getTime()) : null);

            final java.util.Date endDate = entity.getEndDate();
            statement.setDate(5, (endDate != null) ? new Date(endDate.getTime()) : null);

            final java.util.Date createDate = entity.getCreateDate();
            statement.setDate(6, (createDate != null) ? new Date(createDate.getTime()) : null);

            statement.setString(7, entity.getStatus().getName());
            statement.setString(8, entity.getProjectId());
            statement.setString(9, entity.getUserId());
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    void makeUpdateStatement(
            @NotNull final PreparedStatement statement,
            @NotNull final Task entity,
            @NotNull final String userId
    ) {
        try {
            assert entity.getStatus() != null;
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getDescription());

            final java.util.Date startDate = entity.getStartDate();
            statement.setDate(3, (startDate != null) ? new Date(startDate.getTime()) : null);

            final java.util.Date endDate = entity.getEndDate();
            statement.setDate(4, (endDate != null) ? new Date(endDate.getTime()) : null);

            final java.util.Date createDate = entity.getCreateDate();
            statement.setDate(5, (createDate != null) ? new Date(createDate.getTime()) : null);

            statement.setString(6, entity.getStatus().getName());
            statement.setString(7, entity.getProjectId());

            statement.setString(8, entity.getId());
            statement.setString(9, entity.getUserId());
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    void makeSelectAllStatement(
            @NotNull final PreparedStatement statement,
            @Nullable final String userId
    ) {
        assert userId != null;
        try {
            statement.setString(1, userId);
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    @NotNull
    Task parseResultSet(@NotNull final ResultSet resultSet) {
        try {
            @NotNull final Task task = new Task();

            task.setId(resultSet.getString("id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setStartDate(resultSet.getDate("start_date"));
            task.setEndDate(resultSet.getDate("end_date"));
            task.setCreateDate(resultSet.getDate("create_date"));
            task.setStatus(Status.valueOf(resultSet.getString("status")));
            task.setProjectId(resultSet.getString("project_id"));
            task.setUserId(resultSet.getString("user_id"));
            return task;

        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }
}
