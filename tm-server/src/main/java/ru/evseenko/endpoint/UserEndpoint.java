package ru.evseenko.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.service.UserService;
import ru.evseenko.entity.Session;
import ru.evseenko.entity.User;
import ru.evseenko.exception.ServiceException;
import ru.evseenko.service.SessionService;
import ru.evseenko.util.SignatureUtil;

import javax.jws.WebService;
import java.util.*;

@WebService(endpointInterface = "ru.evseenko.api.endpoint.UserEndpoint")
public class UserEndpoint  implements ru.evseenko.api.endpoint.UserEndpoint {
    public UserEndpoint(@NotNull final UserService service, @NotNull final SessionService sessionService) {
        this.sessionService = sessionService;
        this.service = service;
    }

    @NotNull private final UserService service;
    @NotNull private final SessionService sessionService;

    @NotNull
    protected UserService getService() {
        return service;
    }


    @Override
    public void persist(@NotNull final User entity) {
        getService().persist(entity);
    }

    @Override
    public Session userAuthenticate(@NotNull final String login, @NotNull final String passHash) {
        @NotNull final User user = getService().getUserByLogin(login);

        if (!passHash.equals(user.getPasswordHash())) {
            throw new ServiceException("authentication fail");
        }

        @NotNull final Random random = new Random();

        assert user.getId() != null;
        @NotNull final Session session = new Session();
                session.setId(UUID.randomUUID().toString());
                session.setUserUUID(user.getId());
                session.setSignature(Objects.requireNonNull(SignatureUtil.sign(
                user, String.valueOf(random.nextInt(1000000)), random.nextInt(100))));

        sessionService.setUpSession(session);
        return session;
    }

    @Override
    public void userLogout(@NotNull final Session session) {
        sessionService.checkSession(session);
        sessionService.tearDownSession(session);
    }

    @Override
    public @NotNull User get(@NotNull Session session, @NotNull String id) {
        sessionService.checkSession(session);
        if (id.equals(session.getUserUUID())) {
            return getService().get(id);
        } else {
            throw new ServiceException("wrong UUID");
        }
    }

    @Override
    public void update(@NotNull Session session, @NotNull User entity) {
        sessionService.checkSession(session);
        if (session.getUserUUID().equals(entity.getId())) {
            getService().update(entity);
        } else {
            throw new ServiceException("wrong UUID");
        }

    }
}
