package ru.evseenko.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.endpoint.EntityEndpoint;
import ru.evseenko.api.entity.Entity;
import ru.evseenko.api.service.EntityService;
import ru.evseenko.entity.Session;
import ru.evseenko.service.SessionService;

import java.util.List;

public abstract class AbstractEntityEndpoint<T extends Entity> implements EntityEndpoint<T> {
    protected AbstractEntityEndpoint(@NotNull SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @NotNull protected abstract EntityService<T> getService();

    @NotNull protected final SessionService sessionService;

    @NotNull
    @Override
    public T get(@NotNull Session session, @NotNull final String id) {
        sessionService.checkSession(session);
        return getService().get(session.getUserUUID(), id);
    }

    @NotNull
    @Override
    public List<T> getAll(@NotNull final Session session) {
        sessionService.checkSession(session);
        return getService().getAll(session.getUserUUID());
    }

    @Override
    public void update(@NotNull final Session session, @NotNull final T entity) {
        sessionService.checkSession(session);
        getService().update(session.getUserUUID(), entity);
    }

    @Override
    public void persist(@NotNull final Session session, @NotNull final T entity) {
        sessionService.checkSession(session);
        getService().persist(session.getUserUUID(), entity);
    }

    @Override
    public void delete(@NotNull final Session session, @NotNull final T entity) {
        sessionService.checkSession(session);
        getService().delete(session.getUserUUID(), entity);
    }

    @Override
    public void deleteAll(@NotNull final Session session) {
        sessionService.checkSession(session);
        getService().deleteAll(session.getUserUUID());
    }

    @NotNull
    @Override
    public List<T> findByName(@NotNull final Session session, @NotNull final String name) {
        sessionService.checkSession(session);
        return getService().findByName(session.getUserUUID(), name);
    }

    @NotNull
    @Override
    public List<T> findByDescription(@NotNull final Session session, @NotNull final String description) {
        sessionService.checkSession(session);
        return getService().findByDescription(session.getUserUUID(), description);
    }
}
