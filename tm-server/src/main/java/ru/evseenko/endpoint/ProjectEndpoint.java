package ru.evseenko.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.endpoint.EntityEndpoint;
import ru.evseenko.api.service.ProjectService;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Session;
import ru.evseenko.service.SessionService;

import javax.jws.WebService;

@WebService(endpointInterface = "ru.evseenko.api.endpoint.ProjectEndpoint")
public class ProjectEndpoint extends AbstractEntityEndpoint<Project> implements EntityEndpoint<Project>, ru.evseenko.api.endpoint.ProjectEndpoint {
    public ProjectEndpoint(@NotNull final ProjectService service, @NotNull final SessionService sessionService) {
        super(sessionService);
        this.service = service;
    }

    @NotNull private final ProjectService service;

    @NotNull
    @Override
    protected ProjectService getService() {
        return service;
    }

    @Override
    public void delete(@NotNull final Session session, @NotNull final Project entity) {
        sessionService.checkSession(session);
        getService().delete(session.getUserUUID(), entity);
    }

    @Override
    public void deleteAll(@NotNull final Session session) {
        sessionService.checkSession(session);
        getService().deleteAll(session.getUserUUID());
    }
}
