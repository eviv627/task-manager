package ru.evseenko.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.endpoint.EntityEndpoint;
import ru.evseenko.api.service.TaskService;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Session;
import ru.evseenko.entity.Task;
import ru.evseenko.service.SessionService;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.evseenko.api.endpoint.TaskEndpoint")
public class TaskEndpoint extends AbstractEntityEndpoint<Task> implements EntityEndpoint<Task>, ru.evseenko.api.endpoint.TaskEndpoint {
    public TaskEndpoint(@NotNull final TaskService service, @NotNull final SessionService sessionService) {
        super(sessionService);
        this.service = service;
    }

    @NotNull private final TaskService service;

    @NotNull
    @Override
    protected TaskService getService() {
        return service;
    }

    @NotNull
    @Override
    public List<Task> getTaskForProject(@NotNull final Session session, @NotNull final Project project) {
        sessionService.checkSession(session);
        return getService().getTaskForProject(session.getUserUUID(), project);
    }

    @Override
    public void deleteAllRelated(@NotNull final Session session) {
        sessionService.checkSession(session);
        getService().deleteAllRelated(session.getUserUUID());
    }
}
