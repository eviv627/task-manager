package ru.evseenko.api.service;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Task;

import java.util.List;

public interface TaskService extends EntityService<Task> {

    void deleteAllRelated(@NotNull String userId);

    List<Task> getTaskForProject(@NotNull String userId, @NotNull final Project project);
}
