package ru.evseenko.api.service;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface EntityService<T> {

    @NotNull
    T get(@NotNull String userId, @NotNull String id);

    @NotNull
    List<T> getAll(@NotNull String userId);

    @NotNull
    List<T> findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<T> findByDescription(@NotNull String userId, @NotNull String Description);

    void update(@NotNull String userId, @NotNull T entity);

    void persist(@NotNull String userId, @NotNull T entity);

    void delete(@NotNull String userId, @NotNull T entity);

    void deleteAll(@NotNull String userId);
}
