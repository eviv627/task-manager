package ru.evseenko.api.service;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.entity.Project;

public interface ProjectService extends EntityService<Project> {
    void setTaskService(@NotNull final TaskService taskService);
}
