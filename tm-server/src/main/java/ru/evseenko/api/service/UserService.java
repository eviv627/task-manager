package ru.evseenko.api.service;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.entity.User;

public interface UserService {
    User getUserByLogin(@NotNull String login);

    User get(@NotNull String id);

    void update(@NotNull User entity);

    void persist(@NotNull User entity);
}
