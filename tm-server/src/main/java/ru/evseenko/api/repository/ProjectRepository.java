package ru.evseenko.api.repository;

import ru.evseenko.entity.Project;

public interface ProjectRepository extends Repository<Project> {
}
