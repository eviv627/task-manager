package ru.evseenko.api.repository.mybatis;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.repository.Repository;
import ru.evseenko.api.repository.TaskRepository;
import ru.evseenko.entity.Task;

import java.util.List;

public interface TaskRepositoryMyBatis extends TaskRepository {
    @NotNull
    @Results({
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id")
    })
    @Select("SELECT * FROM app_task WHERE id = #{id} AND user_id = #{userId};")
    Task findOne(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @NotNull
    @Results({
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id")
    })
    @Select("SELECT * FROM app_task WHERE user_id = #{userId};")
    List<Task> findAll(@Param("userId") @NotNull String userId);

    @Insert("INSERT INTO app_task (id, name, description, start_date, end_date, create_date, status, project_id, user_id) " +
            "VALUES (#{entity.id}, #{entity.name}, #{entity.description}, #{entity.startDate}, " +
            "#{entity.endDate}, #{entity.createDate}, #{entity.status}, #{entity.projectId}, #{userId})")
    void persist(
            @Param("userId") @NotNull String userId,
            @Param("entity") @NotNull Task entity
    );

    @Update("UPDATE app_task SET name = #{entity.name}, description = #{entity.description}, start_date = #{entity.startDate}, " +
            "end_date = #{entity.endDate}, create_date = #{entity.createDate}, " +
            "status = #{entity.status}, project_id = #{entity.projectId} " +
            "WHERE id = #{entity.id} AND user_id = #{userId}")
    void merge(
            @Param("userId") @NotNull String userId,
            @Param("entity") @NotNull Task entity
    );

    @Delete("DELETE FROM app_task WHERE id = #{entity.id} AND user_id = #{userId}")
    void remove(
            @Param("userId") @NotNull String userId,
            @Param("entity") @NotNull Task entity
    );

    @Delete("DELETE FROM app_task WHERE user_id = #{userId}")
    void removeAll(@Param("userId") @NotNull String userId);
}
