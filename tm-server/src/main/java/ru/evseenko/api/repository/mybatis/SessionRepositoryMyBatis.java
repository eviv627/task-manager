package ru.evseenko.api.repository.mybatis;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.repository.SessionRepository;
import ru.evseenko.entity.Session;

public interface SessionRepositoryMyBatis extends SessionRepository {
    @NotNull
    @Results({
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "userUUID", column = "user_id")
    })
    @Select("SELECT * FROM app_session WHERE id = #{id} AND user_id = #{userId}")
    Session findOne(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Insert("INSERT INTO app_session (id, user_id, signature, create_date) " +
            "VALUES (#{entity.id}, #{userId}, #{entity.signature}, #{entity.createDate})")
    void persist(
            @Param("userId") @NotNull String userId,
            @Param("entity") @NotNull Session entity
    );

    @Delete("DELETE FROM app_session WHERE id = #{entity.id} AND user_id = #{userId}")
    void remove(
            @Param("userId") @NotNull String userId,
            @Param("entity") @NotNull Session entity
    );
}
