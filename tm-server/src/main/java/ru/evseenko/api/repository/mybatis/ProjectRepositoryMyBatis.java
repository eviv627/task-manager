package ru.evseenko.api.repository.mybatis;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.repository.ProjectRepository;
import ru.evseenko.entity.Project;

import java.util.List;

public interface ProjectRepositoryMyBatis extends ProjectRepository {
    @NotNull
    @Results({
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "userId", column = "user_id")
    })
    @Select("SELECT * FROM app_project WHERE id = #{id} and user_id = #{userId}")
    Project findOne(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @NotNull
    @Results({
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "userId", column = "user_id")
    })
    @Select("SELECT * FROM app_project WHERE user_id = #{userId}")
    List<Project> findAll(@Param("userId") @NotNull String userId);

    @Insert("INSERT INTO app_project (id, name, description, start_date, end_date, create_date, status, user_id) " +
            "VALUES (#{entity.id}, #{entity.name}, #{entity.description}, " +
            "#{entity.startDate}, #{entity.endDate}, #{entity.createDate}, #{entity.status}, #{userId})")
    void persist(
            @Param("userId") @NotNull String userId,
            @Param("entity") @NotNull Project entity
    );

    @Update("UPDATE app_project SET name = #{entity.name}, description = #{entity.description}, " +
            "start_date = #{entity.startDate}, end_date = #{entity.endDate}, " +
            "create_date = #{entity.createDate}, status = #{entity.status} " +
            "WHERE id = #{entity.id} AND user_id = #{userId}")
    void merge(
            @Param("userId") @NotNull String userId,
            @Param("entity") @NotNull Project entity
    );

    @Delete("DELETE FROM app_project WHERE id = #{entity.id} AND user_id = #{userId}")
    void remove(
            @Param("userId") @NotNull String userId,
            @Param("entity") @NotNull Project entity
    );

    @Delete("DELETE FROM app_project WHERE user_id = #{userId}")
    void removeAll(@Param("userId") @NotNull String userId);
}
