package ru.evseenko.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.repository.Repository;
import ru.evseenko.entity.User;

import java.util.List;

public interface UserRepository {

    @NotNull
    User findOne(@NotNull String id);

    @NotNull
    List<User> findAll();

    void persist(@NotNull User entity);

    void merge(@NotNull User entity);

    void remove(@NotNull User entity);

}
