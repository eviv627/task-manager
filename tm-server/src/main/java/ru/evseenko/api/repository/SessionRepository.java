package ru.evseenko.api.repository;

import ru.evseenko.api.repository.Repository;
import ru.evseenko.entity.Session;

public interface SessionRepository extends Repository<Session> {
}
