package ru.evseenko.api.repository.mybatis;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.repository.UserRepository;
import ru.evseenko.entity.User;

import java.util.List;

public interface UserRepositoryMyBatis extends UserRepository {

    @NotNull
    @Results({
            @Result(property = "passwordHash", column = "password_hash")
    })
    @Select("SELECT * FROM app_user WHERE id = #{id}")
    User findOne(
            @Param("id") @NotNull String id
    );

    @NotNull
    @Results({
            @Result(property = "passwordHash", column = "password_hash")
    })
    @Select("SELECT * FROM app_user")
    List<User> findAll();

    @Insert("INSERT INTO app_user (id, login, password_hash, role) " +
            "VALUES (#{entity.id}, #{entity.login}, #{entity.passwordHash}, #{entity.role})")
    void persist(
            @Param("entity") @NotNull User entity
    );

    @Update("UPDATE app_user " +
            "SET login = #{entity.login}, password_hash = #{entity.passwordHash}, role = #{entity.role} " +
            "WHERE id = #{entity.id}")
    void merge(
            @Param("entity") @NotNull User entity
    );

    @Delete("DELETE FROM app_user WHERE id = #{entity.id}")
    void remove(
            @Param("entity") @NotNull User entity
    );
}
