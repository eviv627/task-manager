package ru.evseenko.api.repository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface Repository<T> {
    @NotNull
    T findOne(@NotNull String userId, @NotNull String id);

    @NotNull
    List<T> findAll(@NotNull String userId);

    void persist(@NotNull String userId, @NotNull T entity);

    void merge(@NotNull String userId, @NotNull T entity);

    void remove(@NotNull String userId, @NotNull T entity);

    void removeAll(@NotNull String userId);
}
