package ru.evseenko.api.repository;

import ru.evseenko.api.repository.Repository;
import ru.evseenko.entity.Task;

public interface TaskRepository extends Repository<Task> {
}
