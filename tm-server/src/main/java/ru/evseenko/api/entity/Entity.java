package ru.evseenko.api.entity;

public interface Entity extends Identifiable {
    String getName();

    String getDescription();

    String getUserId();
}
