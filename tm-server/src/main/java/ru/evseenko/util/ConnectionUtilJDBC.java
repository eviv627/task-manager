package ru.evseenko.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.exception.RepositoryException;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionUtilJDBC {
    @NotNull private static final String URL;

    @NotNull private static final String USER;

    @NotNull private static final String PASSWORD;

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RepositoryException("No driver class");
        }

        @NotNull final ClassLoader loader = Thread.currentThread().getContextClassLoader();
        @NotNull final Properties property = new Properties();
        try {
            final InputStream inputStream = loader.getResourceAsStream("ru/evseenko/database/database.properties");
            property.load(inputStream);

        } catch (IOException e) {
            throw new RepositoryException("resource not found");
        }

        URL = property.getProperty("database.url");
        USER = property.getProperty("database.user");
        PASSWORD = property.getProperty("database.password");
    }

    @NotNull
    public static Connection getConnection() {
        @Nullable Connection connection;

        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RepositoryException("Connection failed");
        }

        return connection;
    }

    public static void closeConnection(@NotNull final Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new RepositoryException("Connection close failed");
        }
    }
}
