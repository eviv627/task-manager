#**Task Manager**

**Stack:**
java 8, maven 3.3.9

**Developer:**
Evseenko Ivan eviv627@gmail.com

**Command to build:**
`mvn clean package`

**Command to start:**
* server - `java -jar .tm-server/target/release/bin/tm-server-version.jar`
* client - `java -jar .tm-client/target/release/bin/tm-client-version.jar`