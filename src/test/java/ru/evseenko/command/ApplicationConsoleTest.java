package ru.evseenko.command;


import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.evseenko.Application;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApplicationConsoleTest {

    private ByteArrayOutputStream appOut;

    private void setInputStream(String string) {
        System.setIn(new ByteArrayInputStream(string.getBytes()));
    }

    private String filterUUID(String string) {
        String[] strings = string.split("\n");

        Pattern pattern = Pattern.compile("([a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12})");
        Matcher matcher;
        for (int i = 0; i < strings.length; i++) {
            matcher = pattern.matcher(strings[i]);
            if (matcher.find()) {
                String newString = strings[i].replace(matcher.group(0), "{UUID}");
                strings[i] = newString;
            }
        }

        return StringUtils.join(strings, "\n");
    }

    private static String filterWindows(String string) {
        return string.replace("\r\n", "\n").trim();
    }

    @Before
    public void setUpConsoleTest() {
        appOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(appOut));
    }

    @After
    public void setTearDownTest() {
        System.setIn(System.in);
        System.setOut(System.out);
    }

    @Test
    public void helpExitConsoleTest() throws IllegalAccessException, InstantiationException {
        setInputStream("help\n"
                + "exit\n");
        Application.main(new String[0]);
        Assert.assertEquals("*** WELCOME TO TASK MANAGER ***\n" +
                "about - application info\n" +
                "exit - close application\n" +
                "help - show all command\n" +
                "load - load entities\n" +
                "project create - create new project\n" +
                "project delete - delete project\n" +
                "project find - find projects by name or desc\n" +
                "project read - read projects\n" +
                "project read tasks - read project tasks\n" +
                "project show sorted - show sorted projects\n" +
                "project show sorted tasks - show project sorted tasks\n" +
                "project update - update project\n" +
                "save - save entities\n" +
                "task create - create new task\n" +
                "task delete - delete task\n" +
                "task find - find tasks by name or desc\n" +
                "task read - read tasks\n" +
                "task show sorted - show sorted tasks\n" +
                "task update - update task\n" +
                "user login - login user\n" +
                "user logout - logout user\n" +
                "user reg - registration user\n" +
                "user update - view/edit user profile", filterWindows(appOut.toString()));
    }

    @Test
    public void userCreateConsoleTest() throws IllegalAccessException, InstantiationException {
        setInputStream("user reg\n" +
                "Name1\n" +
                "Pass1\n" +
                "exit\n");
        Application.main(new String[0]);
        Assert.assertEquals("*** WELCOME TO TASK MANAGER ***\n" +
                "Enter login :\n" +
                "Enter password :\n" +
                "[OK]", filterWindows(appOut.toString()));
    }

    private static final String LOGIN_INPUT =
            "user reg\n" +
            "Name\n" +
            "Pass\n" +
            "user login\n" +
            "Name\n" +
            "Pass\n";

    private static final String LOGIN_OUTPUT = "*** WELCOME TO TASK MANAGER ***\n" +
            "Enter login :\n" +
            "Enter password :\n" +
            "[OK]\n" +
            "Enter login :\n" +
            "Enter password :\n" +
            "[OK]";

    @Test
    public void userLoginConsoleTest() throws IllegalAccessException, InstantiationException {
        setInputStream("user reg\n" +
                "Name\n" +
                "Pass\n" +
                "exit\n");
        Application.main(new String[0]);
        Assert.assertEquals("*** WELCOME TO TASK MANAGER ***\n" +
                "Enter login :\n" +
                "Enter password :\n" +
                "[OK]", filterWindows(appOut.toString()));
    }

    @Test
    public void projectCreateConsoleTest() throws IllegalAccessException, InstantiationException {
        setInputStream(LOGIN_INPUT +
                "project create\n" +
                "Name\n" +
                "Desc\n" +
                "\n" +
                "\n" +
                "project read\n" +
                "exit\n");
        Application.main(new String[0]);
        Assert.assertEquals(LOGIN_OUTPUT + "\n" + "Enter name :\n" +
                "Enter description :\n" +
                "Enter start date : [pattern: \"dd.MM.yyyy\"]\n" +
                "Cannot read date set null\n" +
                "Enter end date : [pattern: \"dd.MM.yyyy\"]\n" +
                "Cannot read date set null\n" +
                "[OK]\n" +
                "Name, Description, Start date, End Date, Status, UUID\n" +
                "Name, Desc, null, null, planned, {UUID}", filterWindows(filterUUID(appOut.toString())));
    }
}
