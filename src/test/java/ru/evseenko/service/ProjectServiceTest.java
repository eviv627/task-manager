package ru.evseenko.service;

import org.junit.Assert;
import org.junit.Test;
import ru.evseenko.entity.Project;
import ru.evseenko.repository.ProjectRepository;
import ru.evseenko.util.Sort;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectServiceTest {

    @Test
    public void serializationTest() throws IOException, ClassNotFoundException {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);

        List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Project project = new Project();
            project.setName("Name-" + i);
            project.setDescription("Desc-" + i);
            project.setId(UUID.randomUUID().toString());
            projects.add(project);
        }

        for (Project project : projects) {
            projectService.persist(project);
        }

        projectService.serializatonSave();

        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);

        projectService.serializatonLoad();

        List<Project> projectsLoad = projectService.getAll();

        Sort.sortEntities(projects, Sort.NAME);
        Sort.sortEntities(projectsLoad, Sort.NAME);

        Assert.assertEquals(projects, projectsLoad);
    }

    @Test
    public void serializationXMLTest() throws JAXBException, IOException {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);

        List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Project project = new Project();
            project.setName("Name-" + i);
            project.setDescription("Desc-" + i);
            project.setId(UUID.randomUUID().toString());
            projects.add(project);
        }

        for (Project project : projects) {
            projectService.persist(project);
        }

        projectService.xmlJaxbSave();

        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);

        projectService.xmlJaxbLoad();

        List<Project> projectsLoad = projectService.getAll();

        Sort.sortEntities(projects, Sort.NAME);
        Sort.sortEntities(projectsLoad, Sort.NAME);

        Assert.assertEquals(projects, projectsLoad);
    }

    @Test
    public void serializationJSONTest() throws IOException, JAXBException {

        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);

        List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Project project = new Project();
            project.setName("Name-" + i);
            project.setDescription("Desc-" + i);
            project.setId(UUID.randomUUID().toString());
            projects.add(project);
        }

        for (Project project : projects) {
            projectService.persist(project);
        }

        projectService.jsonSave();

        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);

        projectService.jsonLoad();

        List<Project> projectsLoad = projectService.getAll();

        Sort.sortEntities(projects, Sort.NAME);
        Sort.sortEntities(projectsLoad, Sort.NAME);

        Assert.assertEquals(projects, projectsLoad);
    }

    @Test
    public void serializationJsonJAXBTest() throws IOException, JAXBException {

        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);

        List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Project project = new Project();
            project.setName("Name-" + i);
            project.setDescription("Desc-" + i);
            project.setId(UUID.randomUUID().toString());
            projects.add(project);
        }

        for (Project project : projects) {
            projectService.persist(project);
        }

        projectService.jsonMoxySave();

        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);

        projectService.jsonMoxyLoad();

        List<Project> projectsLoad = projectService.getAll();

        Sort.sortEntities(projects, Sort.NAME);
        Sort.sortEntities(projectsLoad, Sort.NAME);

        Assert.assertEquals(projects, projectsLoad);
    }

    @Test
    public void serializationXmlJacksonTest() throws IOException, JAXBException {

        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);

        List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Project project = new Project();
            project.setName("Name-" + i);
            project.setDescription("Desc-" + i);
            project.setId(UUID.randomUUID().toString());
            projects.add(project);
        }

        for (Project project : projects) {
            projectService.persist(project);
        }

        projectService.xmlJacksonSave();

        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);

        projectService.xmlJacksonLoad();

        List<Project> projectsLoad = projectService.getAll();

        Sort.sortEntities(projects, Sort.NAME);
        Sort.sortEntities(projectsLoad, Sort.NAME);

        Assert.assertEquals(projects, projectsLoad);
    }
}
