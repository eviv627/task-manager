package ru.evseenko.service;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Status;
import ru.evseenko.util.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;


public class SortTest {

    private static List<Project> projects;
    private static final int ITER = 1000;
    private static String[] sortedStrings;

    @BeforeClass
    public static void prepareListProjects() {
        projects = new ArrayList<>();
        generateSortedStrings();
        for (int i = 0; i < ITER; i++) {
            Project project = new Project();
            project.setName("-Name-" + sortedStrings[i]);
            project.setDescription("Desc-" + sortedStrings[i]);
            project.setCreateDate(new Date(ITER*i));
            project.setStartDate(new Date(ITER*i));
            project.setEndDate(new Date(ITER*(i+1)));
            if (i <= ITER/2) {
                project.setStatus(Status.PLANNED);
            }
            if (i > ITER/2 && i < (ITER/2 + ITER/4)) {
                project.setStatus(Status.IN_PROGRESS);
            }
            if (i >= (ITER/2 + ITER/4)) {
                project.setStatus(Status.DONE);
            }
            project.setId(UUID.randomUUID().toString());
            projects.add(project);
        }
    }

    private static void generateSortedStrings() {
        sortedStrings = new String[ITER];
        for (int i = 0; i < sortedStrings.length; i++) {
            sortedStrings[i] = Integer.toString(i);
        }
        Arrays.sort(sortedStrings);
    }

    @Test
    public void startDateSortTest(){
        List<Project> projectsNotSort = new ArrayList<>(projects);
        Collections.shuffle(projectsNotSort);
        Assert.assertNotEquals(projects, projectsNotSort);

        Sort.sortEntities(projectsNotSort, Sort.START_DATE);
        Assert.assertEquals(projects, projectsNotSort);
    }

    @Test
    public void endDateSortTest(){
        List<Project> projectsNotSort = new ArrayList<>(projects);
        Collections.shuffle(projectsNotSort);
        Assert.assertNotEquals(projects, projectsNotSort);

        Sort.sortEntities(projectsNotSort, Sort.END_DATE);
        Assert.assertEquals(projects, projectsNotSort);
    }

    @Test
    public void createDateSortTest(){
        List<Project> projectsNotSort = new ArrayList<>(projects);
        Collections.shuffle(projectsNotSort);
        Assert.assertNotEquals(projects, projectsNotSort);

        Sort.sortEntities(projectsNotSort, Sort.CREATE);
        Assert.assertEquals(projects, projectsNotSort);
    }

    @Test
    public void readySortTest(){
        List<Project> projectsNotSort = new ArrayList<>(projects);
        Collections.shuffle(projectsNotSort);
        Assert.assertNotEquals(projects, projectsNotSort);

        Sort.sortEntities(projectsNotSort, Sort.READY);
        Assert.assertEquals(projects, projectsNotSort);
    }

    @Test
    public void nameSortTest(){
        List<Project> projectsNotSort = new ArrayList<>(projects);
        Collections.shuffle(projectsNotSort);
        Assert.assertNotEquals(projects, projectsNotSort);

        Sort.sortEntities(projectsNotSort, Sort.NAME);
        Assert.assertEquals(projects, projectsNotSort);
    }
}
