package ru.evseenko.service;

import org.junit.Assert;
import org.junit.Test;
import ru.evseenko.entity.User;
import ru.evseenko.repository.UserRepository;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;


public class UserServiceTest {

    private static Comparator<User> comparator = Comparator.comparing(User::getLogin);

    @Test
    public void serializationTest() throws IOException, ClassNotFoundException {
        UserRepository userRepository = new UserRepository();
        UserService userService = new UserService(userRepository);

        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setLogin("Name-" + i);
            user.setId(UUID.randomUUID().toString());
            users.add(user);
        }

        for (User user : users) {
            userService.persist(user);
        }

        userService.serializatonSave();

        userRepository = new UserRepository();
        userService = new UserService(userRepository);

        userService.serializatonLoad();

        List<User> usersLoad = userService.getAll();

        users.sort(comparator);
        usersLoad.sort(comparator);

        Assert.assertEquals(users, usersLoad);
    }

    @Test
    public void serializationXMLTest() throws JAXBException, IOException {
        UserRepository userRepository = new UserRepository();
        UserService userService = new UserService(userRepository);

        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setLogin("Name-" + i);
            user.setId(UUID.randomUUID().toString());
            users.add(user);
        }

        for (User user : users) {
            userService.persist(user);
        }

        userService.xmlJaxbSave();

        userRepository = new UserRepository();
        userService = new UserService(userRepository);

        userService.xmlJaxbLoad();

        List<User> usersLoad = userService.getAll();

        users.sort(comparator);
        usersLoad.sort(comparator);

        Assert.assertEquals(users, usersLoad);
    }

    @Test
    public void serializationJSONTest() throws IOException, JAXBException {

        UserRepository userRepository = new UserRepository();
        UserService userService = new UserService(userRepository);

        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setLogin("Name-" + i);
            user.setId(UUID.randomUUID().toString());
            users.add(user);
        }

        for (User user : users) {
            userService.persist(user);
        }

        userService.jsonSave();

        userRepository = new UserRepository();
        userService = new UserService(userRepository);

        userService.jsonLoad();

        List<User> usersLoad = userService.getAll();

        users.sort(comparator);
        usersLoad.sort(comparator);

        Assert.assertEquals(users, usersLoad);
    }

    @Test
    public void serializationJsonJAXBTest() throws IOException, JAXBException {

        UserRepository userRepository = new UserRepository();
        UserService userService = new UserService(userRepository);

        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setLogin("Name-" + i);
            user.setId(UUID.randomUUID().toString());
            users.add(user);
        }

        for (User user : users) {
            userService.persist(user);
        }

        userService.jsonMoxySave();

        userRepository = new UserRepository();
        userService = new UserService(userRepository);

        userService.jsonMoxyLoad();

        List<User> usersLoad = userService.getAll();

        users.sort(comparator);
        usersLoad.sort(comparator);

        Assert.assertEquals(users, usersLoad);
    }
}
