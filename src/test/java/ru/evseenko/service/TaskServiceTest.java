package ru.evseenko.service;

import org.junit.Assert;
import org.junit.Test;
import ru.evseenko.entity.Task;
import ru.evseenko.repository.TaskRepository;
import ru.evseenko.util.Sort;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    @Test
    public void serializationTest() throws IOException, ClassNotFoundException {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);

        List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Task task = new Task();
            task.setName("Name-" + i);
            task.setDescription("Desc-" + i);
            task.setId(UUID.randomUUID().toString());
            tasks.add(task);
        }

        for (Task task : tasks) {
            taskService.persist(task);
        }

        taskService.serializatonSave();

        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);

        taskService.serializatonLoad();

        List<Task> tasksLoad = taskService.getAll();

        Sort.sortEntities(tasks, Sort.NAME);
        Sort.sortEntities(tasksLoad, Sort.NAME);

        Assert.assertEquals(tasks, tasksLoad);
    }

    @Test
    public void serializationXMLTest() throws JAXBException, IOException {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);

        List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Task task = new Task();
            task.setName("Name-" + i);
            task.setDescription("Desc-" + i);
            task.setId(UUID.randomUUID().toString());
            tasks.add(task);
        }

        for (Task task : tasks) {
            taskService.persist(task);
        }

        taskService.xmlJaxbSave();

        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);

        taskService.xmlJaxbLoad();

        List<Task> tasksLoad = taskService.getAll();

        Sort.sortEntities(tasks, Sort.NAME);
        Sort.sortEntities(tasksLoad, Sort.NAME);

        Assert.assertEquals(tasks, tasksLoad);
    }

    @Test
    public void serializationJSONTest() throws IOException, JAXBException {

        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);

        List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Task task = new Task();
            task.setName("Name-" + i);
            task.setDescription("Desc-" + i);
            task.setId(UUID.randomUUID().toString());
            tasks.add(task);
        }

        for (Task task : tasks) {
            taskService.persist(task);
        }

        taskService.jsonSave();

        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);

        taskService.jsonLoad();

        List<Task> tasksLoad = taskService.getAll();

        Sort.sortEntities(tasks, Sort.NAME);
        Sort.sortEntities(tasksLoad, Sort.NAME);

        Assert.assertEquals(tasks, tasksLoad);
    }

    @Test
    public void serializationJsonJAXBTest() throws IOException, JAXBException {

        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);

        List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Task task = new Task();
            task.setName("Name-" + i);
            task.setDescription("Desc-" + i);
            task.setId(UUID.randomUUID().toString());
            tasks.add(task);
        }

        for (Task task : tasks) {
            taskService.persist(task);
        }

        taskService.jsonMoxySave();

        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);

        taskService.jsonMoxyLoad();

        List<Task> tasksLoad = taskService.getAll();

        Sort.sortEntities(tasks, Sort.NAME);
        Sort.sortEntities(tasksLoad, Sort.NAME);

        Assert.assertEquals(tasks, tasksLoad);
    }
}
