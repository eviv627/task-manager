package ru.evseenko.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.evseenko.api.Repository;
import ru.evseenko.entity.User;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;


public class UserRepositoryTest {

    private Repository<User> repository;
    private final static int ITER = 10;

    @Before
    public void setUpTest() {
        repository = new UserRepository();
    }

    @Test
    public void persistSimpleTest() {
        UUID uuid = UUID.randomUUID();
        User user = new User();
        user.setId(uuid.toString());
        user.setLogin("Login");

        repository.persist(user);

        User loadUser = repository.findOne(uuid.toString());
        Assert.assertEquals(user, loadUser);
    }

    @Test
    public void persistManyTest() {
        User[] users = new User[ITER];
        for (int i = 0; i < users.length; i++) {
            users[i] = new User();
            users[i].setLogin("Name-" + i);
            users[i].setId(UUID.randomUUID().toString());
            repository.persist(users[i]);
        }

        List<User> loadUsers = repository.findAll();
        List<User> oldUsers = Arrays.asList(users);

        Comparator<User> comparator = Comparator.comparing(User::getLogin);

        Collections.sort(oldUsers, comparator);
        Collections.sort(loadUsers, comparator);

        Assert.assertEquals(oldUsers, loadUsers);
    }

    @Test
    public void deleteManyTest() {
        User[] users = new User[ITER];
        for (int i = 0; i < users.length; i++) {
            users[i] = new User();
            users[i].setLogin("Name-" + i);
            users[i].setId(UUID.randomUUID().toString());
            repository.persist(users[i]);
            if (i > ITER/2) {
                repository.remove(users[i]);
            }
        }

        List<User> loadUsers = repository.findAll();
        List<User> oldUsers = Arrays.asList(Arrays.copyOf(users,ITER/2 + 1));

        Comparator<User> comparator = Comparator.comparing(User::getLogin);

        Collections.sort(oldUsers, comparator);
        Collections.sort(loadUsers, comparator);

        Assert.assertEquals(oldUsers, loadUsers);
    }

    @Test
    public void updateManyTest() {
        User[] users = new User[ITER];
        for (int i = 0; i < users.length; i++) {
            users[i] = new User();
            users[i].setLogin("Name-" + i);
            users[i].setId(UUID.randomUUID().toString());
            repository.persist(users[i]);
        }

        for (int i = 0; i < users.length; i++) {
            users[i].setLogin("Name-" + i*2);
            repository.merge(users[i]);
        }

        List<User> loadUsers = repository.findAll();
        List<User> oldUsers = Arrays.asList(users);

        Comparator<User> comparator = Comparator.comparing(User::getLogin);

        Collections.sort(oldUsers, comparator);
        Collections.sort(loadUsers, comparator);

        Assert.assertEquals(oldUsers, loadUsers);
    }
}
