package ru.evseenko.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.evseenko.api.Repository;
import ru.evseenko.entity.Task;
import ru.evseenko.util.Sort;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    private Repository<Task> repository;
    private final static int ITER = 10;

    @Before
    public void setUpTest() {
        repository = new TaskRepository();
    }

    @Test
    public void persistSimpleTest() {
        UUID uuid = UUID.randomUUID();
        Task task = new Task();
        task.setId(uuid.toString());
        task.setName("Project name");
        task.setDescription("Desc");

        repository.persist(task);

        Task loadTask = repository.findOne(uuid.toString());
        Assert.assertEquals(task, loadTask);
    }

    @Test
    public void persistManyTest() {
        Task[] tasks = new Task[ITER];
        for (int i = 0; i < tasks.length; i++) {
            tasks[i] = new Task();
            tasks[i].setName("Name-" + i);
            tasks[i].setDescription("Desc-" + i);
            tasks[i].setId(UUID.randomUUID().toString());
            repository.persist(tasks[i]);
        }

        List<Task> loadTasks = repository.findAll();
        List<Task> oldTasks = Arrays.asList(tasks);

        Sort.sortEntities(oldTasks, Sort.NAME);
        Sort.sortEntities(loadTasks, Sort.NAME);

        Assert.assertEquals(oldTasks, loadTasks);
    }

    @Test
    public void deleteManyTest() {
        Task[] tasks = new Task[ITER];
        for (int i = 0; i < tasks.length; i++) {
            tasks[i] = new Task();
            tasks[i].setName("Name-" + i);
            tasks[i].setDescription("Desc-" + i);
            tasks[i].setId(UUID.randomUUID().toString());
            repository.persist(tasks[i]);
            if (i > ITER/2) {
                repository.remove(tasks[i]);
            }
        }

        List<Task> loadTasks = repository.findAll();
        List<Task> oldTasks = Arrays.asList(Arrays.copyOf(tasks,ITER/2 + 1));

        Sort.sortEntities(oldTasks, Sort.NAME);
        Sort.sortEntities(loadTasks, Sort.NAME);

        Assert.assertEquals(oldTasks, loadTasks);
    }

    @Test
    public void updateManyTest() {
        Task[] tasks = new Task[ITER];
        for (int i = 0; i < tasks.length; i++) {
            tasks[i] = new Task();
            tasks[i].setName("Name-" + i);
            tasks[i].setDescription("Desc-" + i);
            tasks[i].setId(UUID.randomUUID().toString());
            repository.persist(tasks[i]);
        }

        for (int i = 0; i < tasks.length; i++) {
            tasks[i].setName("Name-" + i*2);
            tasks[i].setDescription("Desc-" + i*2);
            repository.merge(tasks[i]);
        }

        List<Task> loadTasks = repository.findAll();
        List<Task> oldTasks = Arrays.asList(tasks);

        Sort.sortEntities(oldTasks, Sort.NAME);
        Sort.sortEntities(loadTasks, Sort.NAME);

        Assert.assertEquals(oldTasks, loadTasks);
    }
}
