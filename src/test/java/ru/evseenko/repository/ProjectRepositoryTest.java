package ru.evseenko.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.evseenko.api.Repository;
import ru.evseenko.entity.Project;
import ru.evseenko.util.Sort;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    private Repository<Project> repository;
    private final static int ITER = 10;

    @Before
    public void setUpTest() {
        repository = new ProjectRepository();
    }

    @Test
    public void persistSimpleTest() {
        UUID uuid = UUID.randomUUID();
        Project project = new Project();
        project.setId(uuid.toString());
        project.setName("Project name");
        project.setDescription("Desc");

        repository.persist(project);

        Project loadProject = repository.findOne(uuid.toString());

        Assert.assertEquals(project, loadProject);
    }

    @Test
    public void persistManyTest() {
        Project[] projects = new Project[ITER];
        for (int i = 0; i < projects.length; i++) {
            projects[i] = new Project();
            projects[i].setName("Name-" + i);
            projects[i].setDescription("Desc-" + i);
            projects[i].setId(UUID.randomUUID().toString());
            repository.persist(projects[i]);
        }

        List<Project> loadProjects = repository.findAll();
        List<Project> oldProjects = Arrays.asList(projects);

        Sort.sortEntities(oldProjects, Sort.NAME);
        Sort.sortEntities(loadProjects, Sort.NAME);

        Assert.assertEquals(oldProjects, loadProjects);
    }

    @Test
    public void deleteManyTest() {
        Project[] projects = new Project[ITER];
        for (int i = 0; i < projects.length; i++) {
            projects[i] = new Project();
            projects[i].setName("Name-" + i);
            projects[i].setDescription("Desc-" + i);
            projects[i].setId(UUID.randomUUID().toString());
            repository.persist(projects[i]);
            if (i > ITER/2) {
                repository.remove(projects[i]);
            }
        }

        List<Project> loadProjects = repository.findAll();
        List<Project> oldProjects = Arrays.asList(Arrays.copyOf(projects,ITER/2 + 1));

        Sort.sortEntities(oldProjects, Sort.NAME);
        Sort.sortEntities(loadProjects, Sort.NAME);

        Assert.assertEquals(oldProjects, loadProjects);
    }

    @Test
    public void updateManyTest() {
        Project[] projects = new Project[ITER];
        for (int i = 0; i < projects.length; i++) {
            projects[i] = new Project();
            projects[i].setName("Name-" + i);
            projects[i].setDescription("Desc-" + i);
            projects[i].setId(UUID.randomUUID().toString());
            repository.persist(projects[i]);
        }

        for (int i = 0; i < projects.length; i++) {
            projects[i].setName("Name-" + i*2);
            projects[i].setDescription("Desc-" + i*2);
            repository.merge(projects[i]);
        }

        List<Project> loadProjects = repository.findAll();
        List<Project> oldProjects = Arrays.asList(projects);

        Sort.sortEntities(oldProjects, Sort.NAME);
        Sort.sortEntities(loadProjects, Sort.NAME);

        Assert.assertEquals(oldProjects, loadProjects);
    }
}
