package ru.evseenko;

import ru.evseenko.application.Bootstrap;
import ru.evseenko.command.application.*;
import ru.evseenko.command.project.*;
import ru.evseenko.command.task.*;
import ru.evseenko.command.user.UserLoginCommand;
import ru.evseenko.command.user.UserLogoutCommand;
import ru.evseenko.command.user.UserRegistrationCommand;
import ru.evseenko.command.user.UserUpdateCommand;

import javax.xml.bind.JAXBException;
import java.net.MalformedURLException;

public class Application {

    final static private Class[] COMMANDS = new Class[]{ ProjectFindCommand.class,
            ProjectCreateCommand.class, ProjectDeleteCommand.class, TaskFindCommand.class,
            ProjectShowCommand.class, ProjectShowTasksCommand.class, ProjectShowSortedCommand.class,
            ProjectShowSortedTasksCommand.class, ProjectUpdateCommand.class, TaskCreateCommand.class,
            TaskDeleteCommand.class, TaskShowSortedCommand.class, TaskShowCommand.class,
            TaskUpdateCommand.class, UserLoginCommand.class, UserLogoutCommand.class,
            UserRegistrationCommand.class, UserUpdateCommand.class, ExitCommand.class, HelpCommand.class,
            AboutCommand.class};

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, MalformedURLException, JAXBException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(COMMANDS);
    }
}
