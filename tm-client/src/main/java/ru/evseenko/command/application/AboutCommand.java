package ru.evseenko.command.application;

import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Role;
import ru.evseenko.exception.CommandException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AboutCommand extends AbstractCommand {
    public AboutCommand() {
        super("about", "application info", Role.USER);
    }

    @Override
    public void execute() throws CommandException {
        final ClassLoader loader = Thread.currentThread().getContextClassLoader();
        final Properties property = new Properties();
        try {
            final InputStream inputStream = loader.getResourceAsStream("application.properties");
            property.load(inputStream);

        } catch (IOException e) {
            throw  new CommandException("resource not found");
        }

        final String name = property.getProperty("application.name");
        final String version = property.getProperty("application.version");
        final String build = property.getProperty("application.build");

        System.out.println("App name: " + name +
                ",\n" +
                "App version: " + version +
                ",\n" +
                "App build: " + build);
    }
}
