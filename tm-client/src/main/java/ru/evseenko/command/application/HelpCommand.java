package ru.evseenko.command.application;

import ru.evseenko.command.AbstractCommand;
import ru.evseenko.api.command.Command;
import ru.evseenko.entity.Role;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


public class HelpCommand extends AbstractCommand {

    public HelpCommand() {
        super("help", "show all command", Role.USER);
    }


    @Override
    public void execute() {
        assert context.getCommandMap() != null;
        final List<Map.Entry<String, Command>> commandsSortedList = new ArrayList<>(context.getCommandMap().entrySet());
        commandsSortedList.sort(Comparator.comparing(Map.Entry::getKey));

        for (Map.Entry<String, Command> entry : commandsSortedList){
            System.out.println(entry.getKey() +" - " + entry.getValue().getDescription());
        }
    }
}
