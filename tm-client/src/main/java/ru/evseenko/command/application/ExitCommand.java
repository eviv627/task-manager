package ru.evseenko.command.application;

import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Role;

public class ExitCommand extends AbstractCommand {

    public ExitCommand() {
        super("exit", "close application", Role.USER);
    }

    @Override
    public void execute() {
        assert context.getCommandMap() != null;
        context.getCommandMap().get("logout").execute();
        context.interrupt();
    }
}
