package ru.evseenko.command.user;

import ru.evseenko.command.AbstractCommand;

import ru.evseenko.entity.Role;
import ru.evseenko.entity.User;
import ru.evseenko.exception.CommandException;
import ru.evseenko.service.TerminalService;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class UserRegistrationCommand extends AbstractCommand {

    public UserRegistrationCommand() {
        super("reg", "registration user", Role.USER);
    }

    @Override
    public void execute() {

        final User user = new User();
        final TerminalService terminalService = context.getTerminalService();
        System.out.println("Enter login :");
        assert terminalService != null;
        user.setLogin(terminalService.read());

        System.out.println("Enter password :");
        final MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new CommandException(e);
        }
        messageDigest.reset();
        messageDigest.update(terminalService.read().getBytes(StandardCharsets.UTF_8));
        user.setPasswordHash(Arrays.toString(messageDigest.digest()));

        user.setRole(Role.ADMIN);
        serviceLocator.getUserService().persist(user);
        System.out.println("[OK]");
    }
}
