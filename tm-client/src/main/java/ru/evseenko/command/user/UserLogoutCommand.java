package ru.evseenko.command.user;

import ru.evseenko.command.AbstractCommand;

import ru.evseenko.entity.Role;
import ru.evseenko.entity.Session;

public class UserLogoutCommand extends AbstractCommand {
    public UserLogoutCommand() {
        super("logout", "logout user", Role.USER);
    }

    @Override
    public void execute() {
        assert context.getUserService() != null;
        context.getUserService().userLogout(context.getSession());
        context.setSession(new Session());
        System.out.println("[OK]");
    }
}
