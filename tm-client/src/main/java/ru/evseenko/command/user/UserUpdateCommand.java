package ru.evseenko.command.user;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.endpoint.UserEndpoint;
import ru.evseenko.command.AbstractCommand;

import ru.evseenko.entity.Role;
import ru.evseenko.entity.User;
import ru.evseenko.exception.CommandException;
import ru.evseenko.service.TerminalService;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class UserUpdateCommand extends AbstractCommand {
    public UserUpdateCommand() {
        super("user update", "view/edit user profile", Role.USER);
    }

    @Override
    public void execute() {
        final UserEndpoint userService = serviceLocator.getUserService();
        final TerminalService terminalService = context.getTerminalService();
        final User user = context.getCurrentUser();

        assert user != null;
        assert terminalService != null;
        @NotNull String command;
        System.out.println("Enter new login : { default: " + user.getLogin() + " }");
        command = terminalService.read();
        if (!command.equals("")) {
            user.setLogin(command);
        }

        @NotNull final MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new CommandException(e);
        }

        System.out.println("Enter new password : { default: old }");
        command = terminalService.read();
        if (!command.equals("")) {
            messageDigest.reset();
            messageDigest.update(command.getBytes(StandardCharsets.UTF_8));
            user.setPasswordHash(Arrays.toString(messageDigest.digest()));
        }

        userService.update(context.getSession(), user);
        System.out.println("[OK]");
    }
}
