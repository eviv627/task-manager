package ru.evseenko.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.api.endpoint.UserEndpoint;
import ru.evseenko.command.AbstractCommand;

import ru.evseenko.entity.Role;
import ru.evseenko.entity.Session;
import ru.evseenko.exception.CommandException;
import ru.evseenko.service.TerminalService;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class UserLoginCommand extends AbstractCommand {

    public UserLoginCommand() {
        super("login", "login user", Role.USER);
    }

    @Override
    public void execute() {
        @NotNull final UserEndpoint userService = serviceLocator.getUserService();
        @Nullable final TerminalService terminalService = context.getTerminalService();

        System.out.println("Enter login :");
        assert terminalService != null;
        @NotNull final String login = terminalService.read();

        System.out.println("Enter password :");
        @NotNull final String password = terminalService.read();

        @NotNull final MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new CommandException(e);
        }
        messageDigest.reset();
        messageDigest.update(password.getBytes(StandardCharsets.UTF_8));

        @NotNull final Session session = userService.userAuthenticate(login, Arrays.toString(messageDigest.digest()));

        context.setSession(session);
        System.out.println("[OK]");
    }
}
