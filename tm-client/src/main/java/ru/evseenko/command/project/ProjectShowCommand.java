package ru.evseenko.command.project;

import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Role;

import java.util.List;

public class ProjectShowCommand extends AbstractCommand {

    public ProjectShowCommand() {
        super("project read", "read projects", Role.USER);
    }

    @Override
    public void execute() {
        final List<Project> projects = serviceLocator.getProjectService().getAll(context.getSession());

        System.out.println("Name, Description, Start date, End Date, Status, UUID");
        if (projects.size() == 0) {
            System.out.println("--None--");
        }
        for (Project project : projects) {
            assert context.getTerminalService() != null;
            context.getTerminalService().printProject(project);
        }

    }
}
