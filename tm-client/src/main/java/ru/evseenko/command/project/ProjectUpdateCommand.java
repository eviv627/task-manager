package ru.evseenko.command.project;

import ru.evseenko.api.endpoint.ProjectEndpoint;
import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.Status;
import ru.evseenko.service.TerminalService;

import java.text.ParseException;
import java.util.UUID;

public class ProjectUpdateCommand extends AbstractCommand {

    public ProjectUpdateCommand () {
        super("project update", "update project", Role.ADMIN);
    }

    @Override
    public void execute() {

        final ProjectEndpoint projectService = serviceLocator.getProjectService();
        final TerminalService terminalService = context.getTerminalService();

        System.out.println("Enter Project UUID :");
        assert terminalService != null;
        String command = terminalService.read();
        final Project project = projectService.get(context.getSession(), UUID.fromString(command).toString());

        System.out.println("Enter name : {default : " + project.getName() + " }");
        command = terminalService.read();
        if (!command.equals("")) {
            project.setName(command);
        }

        System.out.println("Enter description : {default : " + project.getDescription() + " }");
        command = terminalService.read();
        if (!command.equals("")) {
            project.setDescription(command);
        }

        System.out.println("Enter start date : {default : "
                + terminalService.dateToString(project.getStartDate()) + " }");
        System.out.println("[pattern: \"" + TerminalService.DATE_FORMAT_PATTERN +"\"]");
        command = terminalService.read();
        if (!command.equals("")) {
            try {
                project.setStartDate(TerminalService.dateFormat.parse(command));
            } catch (ParseException e) {
                System.out.println("Cannot read date, set default");
            }
        }

        System.out.println("Enter end date : {default : "
                + terminalService.dateToString(project.getEndDate()) + " }");
        System.out.println("[pattern: \"" + TerminalService.DATE_FORMAT_PATTERN +"\"]");
        command = terminalService.read();
        if (!command.equals("")) {
            try {
                project.setEndDate(TerminalService.dateFormat.parse(command));
            } catch (ParseException e) {
                System.out.println("Cannot read date, set default");
            }
        }

        assert project.getStatus() != null;
        System.out.println("Select status 0 - Planned, 1 - In progress, 2 - Done { default : " +
                project.getStatus().getName() + " }");
        command = terminalService.read();
        if (!command.equals("")) {
            int num = Integer.parseInt(command);
            for (Status status : Status.values()) {
                if (status.getNum() == num) {
                    project.setStatus(status);
                }
            }
            System.out.println("Set status - " + project.getStatus().getName());
        }

        projectService.update(context.getSession(), project);
        System.out.println("[OK]");
    }
}
