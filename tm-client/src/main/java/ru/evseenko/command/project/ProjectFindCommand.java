package ru.evseenko.command.project;

import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Role;
import ru.evseenko.service.TerminalService;

import java.util.List;

public class ProjectFindCommand extends AbstractCommand {
    public ProjectFindCommand() {
        super("project find", "find projects by name or desc", Role.USER);
    }

    @Override
    public void execute() {

        System.out.println("Enter find method : [name, desc (description)]");
        final TerminalService terminalService = context.getTerminalService();
        assert terminalService != null;
        final String command = terminalService.read();
        final List<Project> projects;
        if ("name".equalsIgnoreCase(command)) {
            System.out.println("Enter name part :");
            projects = serviceLocator.getProjectService().findByName(context.getSession(), terminalService.read());
        } else if ("desc".equalsIgnoreCase(command)) {
            System.out.println("Enter description part :");
            projects = serviceLocator.getProjectService().findByDescription(context.getSession(),
                    terminalService.read());
        } else {
            throw new IllegalArgumentException("illegal find method : " + command);
        }

        System.out.println("Name, Description, Start date, End Date, Status, UUID");
        if (projects.size() == 0) {
            System.out.println("--None--");
        }
        for (Project project : projects) {
            terminalService.printProject(project);
        }
    }
}
