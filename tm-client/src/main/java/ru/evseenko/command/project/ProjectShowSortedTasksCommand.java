package ru.evseenko.command.project;

import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.Task;
import ru.evseenko.service.TerminalService;
import ru.evseenko.util.Sort;

import java.util.List;
import java.util.UUID;

public class ProjectShowSortedTasksCommand extends AbstractCommand {
    public ProjectShowSortedTasksCommand() {
        super("project show sorted tasks", "show project sorted tasks", Role.USER);
    }

    @Override
    public void execute() {

        System.out.println("Enter Project UUID : ");
        Project project;
        final TerminalService terminalService = context.getTerminalService();
        assert terminalService != null;
        final String uuid = terminalService.read();

        try {
            project = serviceLocator.getProjectService().get(context.getSession(), UUID.fromString(uuid).toString());
            final List<Task> tasks = serviceLocator.getTaskService().getTaskForProject(context.getSession(), project);

            System.out.println("Enter sort parameter: [name, start date, end date, create, status]");
            final Sort sort = Sort.getByName(terminalService.read());

            Sort.sortEntities(tasks, sort);

            System.out.println("Name, Description, Start date, End Date, Status, UUID");
            if (tasks.size() == 0) {
                System.out.println("--None--");
            }
            for (Task task : tasks) {
                terminalService.printTask(task);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
