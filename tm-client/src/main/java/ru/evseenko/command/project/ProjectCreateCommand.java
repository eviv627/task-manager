package ru.evseenko.command.project;

import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.Status;
import ru.evseenko.service.TerminalService;

import java.text.ParseException;


public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand () {
        super("project create", "create new project", Role.ADMIN);
    }

    @Override
    public void execute() {
        final Project project = new Project();
        final TerminalService terminalService = context.getTerminalService();

        System.out.println("Enter name :");
        assert terminalService != null;
        project.setName(terminalService.read());

        System.out.println("Enter description :");
        project.setDescription(terminalService.read());

        System.out.println("Enter start date : [pattern: \"" + TerminalService.DATE_FORMAT_PATTERN +"\"]");
        try {
            project.setStartDate(terminalService.readDate());
        } catch (ParseException e) {
            System.out.println("Cannot read date set null");
        }

        System.out.println("Enter end date : [pattern: \"" + TerminalService.DATE_FORMAT_PATTERN +"\"]");
        try {
            project.setEndDate(terminalService.readDate());
        } catch (ParseException e) {
            System.out.println("Cannot read date set null");
        }

        project.setUserId(context.getCurrentUserUUID());
        project.setCreateDate(terminalService.getDate());
        project.setStatus(Status.PLANNED);

        try {
            serviceLocator.getProjectService().persist(context.getSession(), project);
            System.out.println("[OK]");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
