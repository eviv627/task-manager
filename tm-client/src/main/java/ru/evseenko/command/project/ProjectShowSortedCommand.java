package ru.evseenko.command.project;

import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Role;
import ru.evseenko.service.TerminalService;
import ru.evseenko.util.Sort;

import java.util.List;

public class ProjectShowSortedCommand extends AbstractCommand {

    public ProjectShowSortedCommand() {
        super("project show sorted", "show sorted projects", Role.USER);
    }

    @Override
    public void execute() {
        final List<Project> projects = serviceLocator.getProjectService().getAll(context.getSession());

        System.out.println("Enter sort parameter: [name, start date, end date, create, status]");
        final TerminalService terminalService = context.getTerminalService();
        assert terminalService != null;
        final Sort sort = Sort.getByName(terminalService.read());

        Sort.sortEntities(projects, sort);

        System.out.println("Name, Description, Start date, End Date, Status, UUID");
        if (projects.size() == 0) {
            System.out.println("--None--");
        }
        for (Project project : projects) {
            terminalService.printProject(project);
        }

    }
}
