package ru.evseenko.command.project;

import ru.evseenko.api.endpoint.ProjectEndpoint;
import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Role;

import java.util.UUID;

public class ProjectDeleteCommand extends AbstractCommand {

    public ProjectDeleteCommand () {
        super("project delete", "delete project", Role.ADMIN);
    }

    @Override
    public void execute() {

        final ProjectEndpoint projectService = serviceLocator.getProjectService();

        System.out.println("Enter Project UUID : {default all}");
        assert context.getTerminalService() != null;
        final String uuid = context.getTerminalService().read();
        try {
            if (uuid.equals("")) {
                projectService.deleteAll(context.getSession());
                System.out.println("all deleted");
                System.out.println("[OK]");
            } else {
                projectService.delete(context.getSession(),
                        projectService.get(context.getSession(), UUID.fromString(uuid).toString()));
                System.out.println("[OK]");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
