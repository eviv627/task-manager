package ru.evseenko.command.task;

import ru.evseenko.command.AbstractCommand;

import ru.evseenko.entity.Role;
import ru.evseenko.entity.Task;
import ru.evseenko.service.TerminalService;
import ru.evseenko.util.Sort;

import java.util.List;

public class TaskShowSortedCommand extends AbstractCommand {

    public TaskShowSortedCommand() {
        super("task show sorted", "show sorted tasks", Role.USER);
    }

    @Override
    public void execute() {
        final List<Task> tasks = serviceLocator.getTaskService().getAll(context.getSession());
        final TerminalService terminalService = context.getTerminalService();

        System.out.println("Enter sort parameter: [name, start date, end date, create, status]");
        assert terminalService != null;
        final Sort sort = Sort.getByName(terminalService.read());

        Sort.sortEntities(tasks, sort);

        System.out.println("Name, Description, Start date, End Date, Status, UUID");
        if (tasks.size() == 0) {
            System.out.println("--None--");
        }
        for (Task task : tasks) {
            terminalService.printTask(task);
        }

    }
}
