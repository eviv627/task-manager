package ru.evseenko.command.task;

import ru.evseenko.api.endpoint.TaskEndpoint;
import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Role;

import java.util.UUID;

public class TaskDeleteCommand extends AbstractCommand {

    public TaskDeleteCommand () {
        super("task delete", "delete task", Role.ADMIN);
    }

    @Override
    public void execute() {

        final TaskEndpoint taskService = serviceLocator.getTaskService();

        System.out.println("Enter Task UUID : {default all}");
        assert context.getTerminalService() != null;
        final String uuid = context.getTerminalService().read();
        try {
            if (uuid.equals("")) {
                taskService.deleteAll(context.getSession());
                System.out.println("all deleted");
                System.out.println("[OK]");
            } else {
                taskService.delete(context.getSession(),
                        taskService.get(context.getSession(), UUID.fromString(uuid).toString()));
                System.out.println("[OK]");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
