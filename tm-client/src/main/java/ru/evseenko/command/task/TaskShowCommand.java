package ru.evseenko.command.task;

import ru.evseenko.command.AbstractCommand;

import ru.evseenko.entity.Role;
import ru.evseenko.entity.Task;
import ru.evseenko.service.TerminalService;

import java.util.List;

public class TaskShowCommand extends AbstractCommand {

    public TaskShowCommand() {
        super("task read", "read tasks", Role.USER);
    }

    @Override
    public void execute() {
        final List<Task> tasks = serviceLocator.getTaskService().getAll(context.getSession());
        final TerminalService terminalService = context.getTerminalService();

        System.out.println("Name, Description, Start date, End Date, Status, UUID");
        if (tasks.size() == 0) {
            System.out.println("--None--");
        }
        for (Task task : tasks) {
            assert terminalService != null;
            terminalService.printTask(task);
        }
    }
}
