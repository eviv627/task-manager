package ru.evseenko.command.task;

import ru.evseenko.command.AbstractCommand;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.Status;
import ru.evseenko.entity.Task;
import ru.evseenko.service.TerminalService;

import java.text.ParseException;
import java.util.UUID;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand () {
        super("task create", "create new task", Role.ADMIN);
    }

    @Override
    public void execute() {
        final Task task = new Task();
        final TerminalService terminalService = context.getTerminalService();

        System.out.println("Enter name :");
        assert terminalService != null;
        task.setName(terminalService.read());

        System.out.println("Enter description :");
        task.setDescription(terminalService.read());

        System.out.println("Enter start date : [pattern: \"dd-MM-yyyy\"]");
        try {
            task.setStartDate(terminalService.readDate());
        } catch (ParseException e) {
            System.out.println("Cannot read date set null");
        }

        System.out.println("Enter end date : [pattern: \"" + TerminalService.DATE_FORMAT_PATTERN +"\"]");
        try {
            task.setEndDate(terminalService.readDate());
        } catch (ParseException e) {
            System.out.println("Cannot read date set null");
        }

        System.out.println("Enter parent project UUID : ");
        String uuid = terminalService.read();
        try {
            task.setProjectId(UUID.fromString(uuid).toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Cannot read project set null");
        }

        task.setUserId(context.getCurrentUserUUID());

        task.setCreateDate(terminalService.getDate());

        task.setStatus(Status.PLANNED);

        try {
            serviceLocator.getTaskService().persist(context.getSession(), task);
            System.out.println("[OK]");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
