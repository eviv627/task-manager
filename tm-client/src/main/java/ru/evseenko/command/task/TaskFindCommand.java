package ru.evseenko.command.task;

import ru.evseenko.command.AbstractCommand;

import ru.evseenko.entity.Task;
import ru.evseenko.entity.Role;
import ru.evseenko.service.TerminalService;

import java.util.List;

public class TaskFindCommand extends AbstractCommand {
    public TaskFindCommand() {
        super("task find", "find tasks by name or desc", Role.USER);
    }

    @Override
    public void execute() {

        System.out.println("Enter find method : [name, desc (description)]");
        final TerminalService terminalService = context.getTerminalService();
        assert terminalService != null;
        final String command = terminalService.read();
        List<Task> tasks;
        if ("name".equalsIgnoreCase(command)) {
            System.out.println("Enter name part :");
            tasks = serviceLocator.getTaskService().findByName(context.getSession(), terminalService.read());
        } else if ("desc".equalsIgnoreCase(command)) {
            System.out.println("Enter description part :");
            tasks = serviceLocator.getTaskService().findByDescription(context.getSession(), terminalService.read());
        } else {
            throw new IllegalArgumentException("illegal find method : " + command);
        }

        System.out.println("Name, Description, Start date, End Date, Status, UUID");
        if (tasks.size() == 0) {
            System.out.println("--None--");
        }
        for (Task task : tasks) {
            terminalService.printTask(task);
        }
    }
}
