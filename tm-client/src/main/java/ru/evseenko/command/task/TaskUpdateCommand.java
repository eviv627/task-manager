package ru.evseenko.command.task;

import ru.evseenko.api.endpoint.TaskEndpoint;
import ru.evseenko.command.AbstractCommand;

import ru.evseenko.entity.Role;
import ru.evseenko.entity.Status;
import ru.evseenko.entity.Task;
import ru.evseenko.service.TerminalService;

import java.text.ParseException;
import java.util.UUID;

public class TaskUpdateCommand extends AbstractCommand {

    public TaskUpdateCommand () {
        super("task update", "update task", Role.ADMIN);
    }

    @Override
    public void execute() {

        final TaskEndpoint taskService = serviceLocator.getTaskService();
        final TerminalService terminalService = context.getTerminalService();

        System.out.println("Enter Task UUID :");
        assert terminalService != null;
        final String uuid = terminalService.read();
        final Task task = taskService.get(context.getSession(), uuid.trim());
        String command;

        System.out.println("Enter name : {default : " + task.getName() + " }");
        command = terminalService.read();
        if (!command.equals("")) {
            task.setName(command);
        }

        System.out.println("Enter description : {default : " + task.getDescription() + " }");
        command = terminalService.read();
        if (!command.equals("")) {
            task.setDescription(command);
        }

        System.out.println("Enter start date : {default : "
                + terminalService.dateToString(task.getStartDate()) + " }");
        System.out.println("[pattern: \"" + TerminalService.DATE_FORMAT_PATTERN +"\"]");
        command = terminalService.read();
        if (!command.equals("")) {
            try {
                task.setStartDate(TerminalService.dateFormat.parse(command));
            } catch (ParseException e) {
                System.out.println("Cannot command date, set default");
            }
        }

        System.out.println("Enter end date : {default : "
                + terminalService.dateToString(task.getEndDate()) + " }");
        System.out.println("[pattern: \"" + TerminalService.DATE_FORMAT_PATTERN +"\"]");
        command = terminalService.read();
        if (!command.equals("")) {
            try {
                task.setEndDate(TerminalService.dateFormat.parse(command));
            } catch (ParseException e) {
                System.out.println("Cannot command date, set default");
            }
        }

        assert task.getStatus() != null;
        System.out.println("Select status 0 - Planned, 1 - In progress, 2 - Done { default : " +
                task.getStatus().getName() + " }");
        command = terminalService.read();
        if (!command.equals("")) {
            int num = Integer.parseInt(command);
            for (Status status : Status.values()) {
                if (status.getNum() == num) {
                    task.setStatus(status);
                }
            }
            System.out.println("Set status - " + task.getStatus().getName());
        }

        System.out.println("Enter parent project UUID : {default : " + task.getProjectId() + " }");
        command = terminalService.read();
        try {
            task.setProjectId(UUID.fromString(command).toString());
        } catch (Exception e) {
            System.out.println("Cannot command project, set default");
        }

        taskService.update(context.getSession(), task);
        System.out.println("[OK]");
    }
}
