package ru.evseenko.command;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.command.Command;
import ru.evseenko.application.Context;
import ru.evseenko.entity.Role;
import ru.evseenko.exception.CommandException;
import ru.evseenko.api.application.ServiceLocator;


public abstract class AbstractCommand implements Command {
    protected final Role role;
    protected final String name;
    protected final String description;
    protected ServiceLocator serviceLocator;
    protected Context context;

    protected AbstractCommand(String name, String description, Role role) {
        this.name = name;
        this.description = description;
        this.role = role;
    }

    public abstract void execute() throws CommandException;

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setContext(@NotNull Context context) {
        this.context = context;
        this.serviceLocator = context;
    }

    @NotNull
    public Role getAccessRole() {
        return role;
    }
}

