package ru.evseenko.application;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.command.Command;
import ru.evseenko.api.endpoint.ProjectEndpoint;
import ru.evseenko.api.endpoint.TaskEndpoint;
import ru.evseenko.api.endpoint.UserEndpoint;
import ru.evseenko.endpoint.Constant;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.Session;
import ru.evseenko.service.TerminalService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;


public class Bootstrap {

    private Map<String, Command> commandsMap;

    private final URL projectUrl = new URL(Constant.PROJECT_ENDPOINT_URL + "?wsdl");
    private final URL taskUrl = new URL(Constant.TASK_ENDPOINT_URL + "?wsdl");
    private final URL userUrl = new URL(Constant.USER_ENDPOINT_URL + "?wsdl");

    private final QName projectQname = new QName("http://endpoint.evseenko.ru/", "ProjectEndpointService");
    private final QName taskQname = new QName("http://endpoint.evseenko.ru/", "TaskEndpointService");
    private final QName userQname = new QName("http://endpoint.evseenko.ru/", "UserEndpointService");

    private final Service projectServ = Service.create(projectUrl, projectQname);
    private final Service taskServ = Service.create(taskUrl, taskQname);
    private final Service userServ = Service.create(userUrl, userQname);

    private final ProjectEndpoint projectService = projectServ.getPort(ProjectEndpoint.class);
    private final TaskEndpoint taskService = taskServ.getPort(TaskEndpoint.class);
    private final UserEndpoint userService = userServ.getPort(UserEndpoint.class);
    private final TerminalService terminalService = new TerminalService(new Scanner(System.in));
    private final Context context = new Context(new Session());

    public Bootstrap() throws MalformedURLException {
    }

    public void init(final Class[] commands) throws IllegalAccessException, InstantiationException {

        context.setProjectService(projectService);
        context.setTaskService(taskService);
        context.setUserService(userService);
        context.setTerminalService(terminalService);

        commandsMap = new HashMap<>();
        for (Class commandClass : commands) {
            Command command = (Command) commandClass.newInstance();
            command.setContext(context);
            commandsMap.put(command.getName(), command);
        }

        commandsMap = Collections.unmodifiableMap(commandsMap);
        context.setCommandMap(commandsMap);
        start();
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        String command;
        while (!context.isInterrupted()) {
            command = terminalService.read();
            try {
                executeCommand(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void executeCommand(@NotNull final String commandStr) {
        final Command command = commandsMap.get(commandStr);
        if (command != null) {
            checkAccessPermissions(command);
            command.execute();
        } else {
            System.out.println("Wrong input");
        }
    }

    private void checkAccessPermissions(@NotNull final Command command) {
        final Role role = context.getCurrentUserRole();
        if (!(role == Role.ADMIN) && command.getAccessRole() == Role.ADMIN) {
            throw new RuntimeException("Access denied");
        }
    }
}
