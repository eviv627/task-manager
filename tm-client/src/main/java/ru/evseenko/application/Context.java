package ru.evseenko.application;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.api.application.ServiceLocator;
import ru.evseenko.api.command.Command;
import ru.evseenko.api.endpoint.ProjectEndpoint;
import ru.evseenko.api.endpoint.TaskEndpoint;
import ru.evseenko.api.endpoint.UserEndpoint;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.Session;
import ru.evseenko.entity.User;
import ru.evseenko.service.TerminalService;

import java.util.Map;

public class Context implements ServiceLocator {

    public Context(@NotNull final Session session) {
        this.session = session;
    }

    @Getter
    @Setter
    @Nullable
    private ProjectEndpoint projectService;

    @Getter
    @Setter
    @Nullable
    private TaskEndpoint taskService;

    @Getter
    @Setter
    @Nullable
    private UserEndpoint userService;

    @Getter
    @Setter
    @Nullable
    private TerminalService terminalService;

    private boolean isInterrupted = false;

    @Getter
    @Nullable
    private User currentUser;

    @Getter
    @NotNull
    private Session session;

    @Getter
    @Setter
    @Nullable
    private Map<String, Command> commandMap;

    @Nullable
    public String getCurrentUserUUID() {
        return currentUser != null ? currentUser.getId() : null;
    }

    boolean isInterrupted() {
        return isInterrupted;
    }

    public void interrupt() {
        isInterrupted = true;
    }

    @NotNull
    Role getCurrentUserRole() {
        if (currentUser == null || currentUser.getRole() == null) {
            return Role.USER;
        }
        return currentUser.getRole();
    }

    public void setSession(@NotNull final Session session) {
        this.session = session;
        assert userService != null;
        if (!"".equals(session.getUserUUID())) {
            this.currentUser = userService.get(session, session.getUserUUID());
        } else {
            this.currentUser = new User();
        }
    }

}
