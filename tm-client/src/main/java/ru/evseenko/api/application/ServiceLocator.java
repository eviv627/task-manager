package ru.evseenko.api.application;

import ru.evseenko.api.endpoint.ProjectEndpoint;
import ru.evseenko.api.endpoint.TaskEndpoint;
import ru.evseenko.api.endpoint.UserEndpoint;
import ru.evseenko.service.TerminalService;

public interface ServiceLocator {
    ProjectEndpoint getProjectService();
    TaskEndpoint getTaskService();
    UserEndpoint getUserService();
    TerminalService getTerminalService();
}
