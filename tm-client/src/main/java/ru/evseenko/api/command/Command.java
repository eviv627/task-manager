package ru.evseenko.api.command;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.application.Context;
import ru.evseenko.entity.Role;
import ru.evseenko.exception.CommandException;

public interface Command {
    void execute() throws CommandException;

    @NotNull
    String getName();

    @NotNull
    String getDescription();

    void setContext(@NotNull Context context);

    @NotNull
    Role getAccessRole();
}
