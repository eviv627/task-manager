package ru.evseenko.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class TerminalService {
    public static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_PATTERN);

    @Getter
    private final Scanner scanner;

    public TerminalService(Scanner scanner) {
        this.scanner = scanner;
    }

    @NotNull
    public String dateToString(@Nullable final Date date) {
        return date == null ? "null" :  dateFormat.format(date);
    }

    @NotNull
    public String read() {
        return getScanner().nextLine().trim();
    }

    @NotNull
    public Date readDate() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_PATTERN);
        return dateFormat.parse(read());
    }

    @NotNull
    public Date getDate() {
        return new Date(System.currentTimeMillis());
    }

    public void printProject(@NotNull final Project project) {
        String result =
                project.getName() +
                        ", " +
                        project.getDescription()
                        + ", " +
                        dateToString(project.getStartDate()) +
                        ", " +
                        dateToString(project.getEndDate()) +
                        ", " +
                        (project.getStatus() != null ? project.getStatus().getName() : null) +
                        ", " +
                        project.getId();
        System.out.println(result);
    }

    public void printTask(@NotNull final Task task) {
        String result = task.getName() +
                ", " +
                task.getDescription() +
                ", " +
                dateToString(task.getStartDate()) +
                ", " +
                dateToString(task.getEndDate()) +
                ", " +
                (task.getStatus() != null ? task.getStatus().getName() : null) +
                ", " +
                task.getId();
        System.out.println(result);
    }

}
