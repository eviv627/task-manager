package ru.evseenko.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CommandException extends RuntimeException {
    public CommandException(Exception e) {
        super(e);
    }

    public CommandException(String message) {
        super(message);
    }
}
