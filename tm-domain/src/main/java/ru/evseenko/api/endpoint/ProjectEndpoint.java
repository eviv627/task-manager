package ru.evseenko.api.endpoint;

import ru.evseenko.entity.Project;

import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;

@WebService
@XmlSeeAlso({Project.class})
public interface ProjectEndpoint extends EntityEndpoint<Project> {
}
