package ru.evseenko.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Session;
import ru.evseenko.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@WebService
@XmlSeeAlso({Task.class})
public interface TaskEndpoint extends EntityEndpoint<Task> {
    @WebMethod
    void deleteAllRelated(@WebParam(name = "session") @NotNull Session session);
    @WebMethod
    List<Task> getTaskForProject(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "project") @NotNull final Project project
    );
}
