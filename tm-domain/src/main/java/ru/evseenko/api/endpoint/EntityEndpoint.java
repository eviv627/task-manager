package ru.evseenko.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface EntityEndpoint<T> {
    @WebMethod
    @NotNull
    T get(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    @NotNull
    List<T> getAll(@WebParam(name = "session") @NotNull Session session);

    @WebMethod
    @NotNull
    List<T> findByName(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "name") @NotNull String name
    );

    @WebMethod
    @NotNull
    List<T> findByDescription(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    void update(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "entity") @NotNull T entity
    );

    @WebMethod
    void persist(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "entity") @NotNull T entity
    );

    @WebMethod
    void delete(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "entity") @NotNull T entity
    );

    @WebMethod
    void deleteAll(@WebParam(name = "session") @NotNull Session session);
}