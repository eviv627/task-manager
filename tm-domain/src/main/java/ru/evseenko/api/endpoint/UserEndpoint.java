package ru.evseenko.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.entity.Session;
import ru.evseenko.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;

@WebService
@XmlSeeAlso({User.class})
public interface UserEndpoint {

    @WebMethod
    Session userAuthenticate(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "passwordHash") @NotNull String passHash
    );

    @WebMethod
    void userLogout(@WebParam(name = "session") @NotNull Session session);

    @WebMethod
    @NotNull
    User get(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    void update(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "entity") @NotNull User entity
    );

    @WebMethod
    void persist(
            @WebParam(name = "entity") @NotNull User entity
    );
}
