package ru.evseenko.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.entity.Identifiable;

import java.util.Date;

@Getter
@Setter
public class Session implements Identifiable {
    public Session() {
        this.userUUID = "";
        this.signature = "";
        this.id = "";
        this.createDate = new Date();
    }

    @NotNull
    private String id;
    @NotNull
    private String userUUID;
    @NotNull
    private String signature;
    @NotNull
    private Date createDate;
}
