package ru.evseenko.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.api.entity.Entity;
import ru.evseenko.api.entity.SortableEntity;

import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Setter
@Getter
@XmlSeeAlso({Status.class})
@NoArgsConstructor
public class Task implements Entity, SortableEntity, Serializable {
    @Nullable
    private String id;
    @Nullable
    private String projectId;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date startDate;
    @Nullable
    private Date endDate;
    @Nullable
    private Date createDate;
    @Nullable
    private Status status;
    @Nullable
    private String userId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(id, task.id) &&
                Objects.equals(name, task.name) &&
                Objects.equals(description, task.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
