package ru.evseenko.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.api.entity.Entity;

import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.Objects;

@Setter
@Getter
@XmlSeeAlso({Role.class})
@NoArgsConstructor
public class User implements Entity, Serializable {
    @Nullable
    private String id;
    @Nullable
    private String login;
    @Nullable
    private String passwordHash;
    @Nullable
    private Role role;

    @Override
    @JsonIgnore
    @Nullable
    public String getName() {
        return login;
    }

    @Override
    @JsonIgnore
    @Nullable
    public String getDescription() {
        return login;
    }

    @Override
    @JsonIgnore
    public String getUserId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(login, user.login) &&
                role == user.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, role);
    }
}
