package ru.evseenko.entity;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum Status { PLANNED("PLANNED", 0), IN_PROGRESS("IN_PROGRESS", 1), DONE("DONE", 2);
    @Getter
    @NotNull
    private String name;
    @Getter
    private int num;

    Status(@NotNull String name, int num) {
        this.name = name;
        this.num = num;
    }
}
