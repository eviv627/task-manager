package ru.evseenko.endpoint;

public class Constant {
    public static final String PROJECT_ENDPOINT_URL = "http://localhost:8080/ProjectEndpoint";
    public static final String TASK_ENDPOINT_URL = "http://localhost:8080/TaskEndpoint";
    public static final String USER_ENDPOINT_URL = "http://localhost:8080/UserEndpoint";
}
